import { Injectable } from '@angular/core';

declare let AzureStorage;
declare let window;

/*
  Generated class for the BlobServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BlobServiceProvider {

  constructor() {
    console.log('Hello BlobServiceProvider Provider');
  }

  getBlobService(account, accessToken) {
    let blobUri = 'https://' + account + '.blob.core.windows.net';

    let tokenCredential = new AzureStorage.Blob.TokenCredential(accessToken);
    let blobService = AzureStorage.Blob.createBlobServiceWithTokenCredential(blobUri, tokenCredential);

    return blobService;
  }

  uploadBlob(account, accessToken, container, blobName, file, fnCallBack) {
    let blobService = this.getBlobService(account, accessToken);
    let customBlockSize = file.size > 1024 * 1024 * 32 ? 1024 * 1024 * 4 : 1024 * 512;

    blobService.createBlockBlobFromBrowserFile(container, blobName, file, { blockSize: customBlockSize }, function (error, result, response) {
      fnCallBack(error);
    });
  }

  async deleteBlob(account, accessToken, container, blobName) {
    let blobService = this.getBlobService(account, accessToken);
    if (!blobService) {
      console.error('blobService is not exist');
      return;
    }

    await blobService.deleteBlobIfExists(container, blobName, function (error, result) {
      if (error) {
        console.error('Delete blob failed, open brower console for more detailed info.');
      } else {
        console.log('Delete  successfully!');
      }
    });
  }

  downloadBlob(account, accessToken, container, blob, fnCallBack) {
    let blobService = this.getBlobService(account, accessToken);
    let blobUri = 'https://' + account + '.blob.core.windows.net';

    let readStream = blobService.createReadStream(container, blob, (err, responseBlob, response) => {
      // console.log(response);
      if (err) {
        console.error("Couldn't download blob %s", responseBlob);
        console.error(err);
      } else {
        console.log("Sucessfully downloaded blob %s", responseBlob);
      }
    });

    readStream.on('data', data => {
      let Blobimg = new Blob([data], { type: "image/jpeg" });

      let reader = new FileReader();
      reader.readAsDataURL(Blobimg); 
      reader.onloadend = function() {
        let base64data = reader.result;
        fnCallBack(base64data);
      }
    });

  }

  getBlobList(account, accessToken, container, callback) {
    let rlt = [];
    let blobService = this.getBlobService(account, accessToken);
    blobService.listBlobsSegmented(container, null, (error, results) => {
      if (error) {
        // List blobs error
        console.error('List blobs error');
      } else {
        console.log('results', results);
        for (var i = 0, blob; blob = results.entries[i]; i++) {
          rlt.push(blob.name);
        }
      }

      callback(rlt);
      console.log('getBlobList-rlt await');
    });
  }
}
