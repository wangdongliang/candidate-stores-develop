import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the P5_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var p5: any;

@IonicPage()
@Component({
  selector: 'page-p5-2',
  templateUrl: 'p5-2.html',
})
export class P5_2Page {

  myp5: any;
  cnv: any;
  pObj: any;
  slider: any;
  text: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad P5_2Page');
  }
  ionViewDidLeave() {
    this.pObj.remove();
  }
  rotatePicture(param, drag?) {
    let p5obj = (p) => {
      p.setup = (() => {
        if (this.pObj != undefined) {
          this.pObj.remove();
        }
        this.pObj = p;
        this.cnv = p.createCanvas(250, 500);
        // id="myCanvas"と紐づけ
        this.cnv.parent('myCanvas');
        this.slider = p.createSlider(0, 500, 0, 10);
        this.slider.position(50, 600);
        this.text = p.createInput();
        this.text.position(50, 625);
      });
      p.draw = () => {
        let x = (p.width - 125) / 2 + 125 / 2;
        let y = (p.height - 90) / 2 + 90 / 2;

        p.background(220);// 背景色
        switch (param) {
          case 'triangle':
            //三角形の中心点の座標の計算方法？？？
            // p.push(); // オブジェクト作成
            p.translate(x, y);
            p.rotate(this.rotateCompute(p));
            p.triangle(0, 0, 58, 20, 86, 75);
            // this.polygon(0, 0, 82, 3, p); // 関数
            // p.pop(); // オブジェクト完成
            break;
          case 'rect':
            // p.push();
            p.rectMode(p.CENTER);
            p.translate(x, y);
            p.rotate(this.rotateCompute(p));
            p.rect(0, 0, this.slider.value() + 80, this.slider.value() + 50);
            // p.pop();
            break;
          case 'polygon':
            p.translate(x, y);
            p.text('hello', x, y);
            p.textSize(32);
            p.rotate(this.rotateCompute(p));
            this.polygon(0, 0, this.slider.value(), 5, p)
            break;
        }
      }
    }
    // インスタンスモード
    this.myp5 = new p5(p5obj);

  }
  //多角形の作成
  polygon(x, y, radius, npoints, pobj) {
    var angle = pobj.TWO_PI / npoints;
    pobj.beginShape(); // 形を作る
    for (var a = 0; a < pobj.TWO_PI; a += angle) {　// TWO_PIは円の円周率。
      var sx = x + pobj.cos(a) * radius;
      var sy = y + pobj.sin(a) * radius;
      pobj.vertex(sx, sy);
    }
    pobj.endShape(pobj.CLOSE); // 形を作り終える
  }

  //回転角度の計算
  rotateCompute(pobj) {
    return pobj.atan2(pobj.mouseY - pobj.height / 2, pobj.mouseX - pobj.width / 2);
  }

  //リセット
  reset() {
    if (this.pObj != undefined) {
      this.pObj.remove();
    }
  }
}
