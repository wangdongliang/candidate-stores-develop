import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelDataPage } from './sel-data';

@NgModule({
  declarations: [
    SelDataPage,
  ],
  imports: [
    IonicPageModule.forChild(SelDataPage),
  ],
})
export class SelDataPageModule {}
