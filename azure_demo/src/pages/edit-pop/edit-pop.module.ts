import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditPopPage } from './edit-pop';

@NgModule({
  declarations: [
    EditPopPage,
  ],
  imports: [
    IonicPageModule.forChild(EditPopPage),
  ],
})
export class EditPopPageModule {}
