import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { P5MenuPage } from './p5-menu';

@NgModule({
  declarations: [
    P5MenuPage,
  ],
  imports: [
    IonicPageModule.forChild(P5MenuPage),
  ],
})
export class P5MenuPageModule {}
