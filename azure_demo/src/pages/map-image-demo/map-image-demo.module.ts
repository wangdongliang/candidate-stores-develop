import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapImageDemoPage } from './map-image-demo';

@NgModule({
  declarations: [
    MapImageDemoPage,
  ],
  imports: [
    IonicPageModule.forChild(MapImageDemoPage),
  ],
})
export class MapImageDemoPageModule {}
