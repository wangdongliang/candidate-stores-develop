import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { LoadingController } from 'ionic-angular';


declare let google;

/**
 * Generated class for the CalculateDemoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calculate-demo',
  templateUrl: 'calculate-demo.html',
})
export class CalculateDemoPage {

  //Googlemap表示オブジェクト
  @ViewChild('map') mapElement: ElementRef;
  private map: any;
  private polygon: any;
  private listener: any;
  private directionsDisplay: any;

  private ploygonArr = [];
  private ploylineArr = [];
  private listenerArr = [];
  private infoWindowArr = [];
  private markArr = [];
  private isDisplay = 'block';


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
  ionViewDidLoad() {
    this.initMap();
  }
  
  private initMap() {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      center: {lat: 35.459471, lng: 139.627371},
      // dhcのposition
      // center: {lat: 38.8544301284, lng: 121.4829879038},
      zoom: 18,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false,
      clickableIcons: false
    });
  }

  public drawPolygon() {
    // this.cleanAll();
    let polygon = new google.maps.Polygon({
      map: this.map,
      editable: true
    });

    if (this.listener) {
      this.listener.remove();
    }
    this.listener = this.map.addListener('click', (e)=> {
      console.log('polygon.getEditable():', polygon.getEditable());
      polygon.getPath().push(e.latLng);
    });
    
    this.ploygonArr.push(polygon);
    this.listenerArr.push(this.listener);
  }
  
  public calculatePolygon() {
    this.cleanInfoWindow();
    this.ploygonArr.forEach((polygon)=> {
      let latLangArr = polygon.getPath();
      let len = latLangArr.getLength();
      if (len <= 0) {
        return;
      }
  
      // 面積の計測
      let area = google.maps.geometry.spherical.computeArea(latLangArr);
      // let area1 = google.maps.geometry.spherical.computeSignedArea(latLangArr);
      let infoWindow = new google.maps.InfoWindow();
      infoWindow.open(this.map);
      infoWindow.setPosition(latLangArr.getAt(len -1));
      infoWindow.setContent('該当のゾーン面積：' + area + ' M^');
      this.infoWindowArr.push(infoWindow);
    });
  }

  public cleanInfoWindow() {
    this.infoWindowArr.forEach(info => {
      info.close();
    });

    this.infoWindowArr = [];
  }

  public cleanAll() {
    this.ploygonArr.forEach(element => {
      element.setMap(null);  
    });

    this.ploylineArr.forEach(element => {
      element.setMap(null);  
    });

    this.listenerArr.forEach(element => {
      element.remove();  
    });

    this.markArr.forEach(element => {
      element.setMap(null);  
    });

    this.ploygonArr = [];
    this.ploylineArr = [];
    this.listenerArr = [];
    this.cleanInfoWindow();
    if (this.directionsDisplay) {
      this.directionsDisplay.setMap(null);
    }

    this.markArr = [];
  }
  
  drawPolyline() {
    // this.cleanAll();
    let polyline = new google.maps.Polyline({
      map: this.map,
    });

    if (this.listener) {
      this.listener.remove();
    }
    this.listener = this.map.addListener('click', (e)=> {
      polyline.getPath().push(e.latLng);
    });
    
    this.ploylineArr.push(polyline);
    this.listenerArr.push(this.listener);
  }

  calculatePolyline() {
    this.cleanInfoWindow();
    this.ploylineArr.forEach((polyline)=> {
      let latLangArr = polyline.getPath().getArray();
      let len = latLangArr.length;
      if (len <= 1) {
        return;
      }
  
      for (let i=0; i<len-1; i++) {
        let poinA = latLangArr[i];
        let pintB = latLangArr[i+1];
        console.log('poinA', poinA);
        console.log('pintB', pintB);
        let distance = google.maps.geometry.spherical.computeDistanceBetween(poinA, pintB);
        let infoWindow = new google.maps.InfoWindow();
        infoWindow.open(this.map);
        infoWindow.setPosition(poinA);
        infoWindow.setContent('両点のDistance：' +  distance + ' M');
        this.infoWindowArr.push(infoWindow);
      }

      let length = google.maps.geometry.spherical.computeLength(polyline.getPath());
      let infoWindow = new google.maps.InfoWindow();
      infoWindow.open(this.map);
      infoWindow.setPosition(latLangArr[latLangArr.length-1]);
      infoWindow.setContent('Distanceの総計：' +  length + ' M');
      this.infoWindowArr.push(infoWindow);
    });
  }

  drawRute() {
    let directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      draggable: true,
      map: this.map,
      hideRouteList: false
    });

    let start = null;
    if (this.listener) {
      this.listener.remove();
    }
    this.listener = this.map.addListener('click', (event)=>{
      console.log('You clicked on: ' + event.latLng);
      if (!start) {
        start = event.latLng;
        return;
      }
      // Calling e.stop() on the event prevents the default info window from
      // showing.
      // If you call stop here when there is no placeId you will prevent some
      // other map click event handlers from receiving the event.
      event.stop();

      directionsService.route({
        origin: start,
        destination: event.latLng,
        travelMode: 'WALKING'
      }, (response, status)=> {
        console.log('directionsService response:', response);
        if (status === 'OK') {
          this.directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
    });

    this.directionsDisplay.addListener('directions_changed', (e) =>{
      console.log('Directions:', this.directionsDisplay.getDirections());
      this.cleanInfoWindow();
    })
  }

  calculateRute() {
    if (!this.directionsDisplay) {
      return;
    }
    let direc = this.directionsDisplay.getDirections();
    if (direc) {
      direc.routes.forEach(route => {
        console.log('distance 単位(メートル):', route.legs[0].distance.value);
        console.log('duration 単位(秒):', route.legs[0].duration.value);

        let paths = route.overview_path;
        console.log('overview_path:', route.overview_path);

        let infoWindow = new google.maps.InfoWindow();
        infoWindow.open(this.map);
        infoWindow.setPosition(paths[paths.length-1]);
        infoWindow.setContent(route.legs[0].distance.value + ' meter,' + route.legs[0].duration.value + '秒');
        this.infoWindowArr.push(infoWindow);
      });
    }
  }

  public getElevation() {
    
    this.cleanAll();
    let eleva = new google.maps.ElevationService();

    if (this.listener) {
        this.listener.remove();
    }
    
    let polyline = new google.maps.Polyline({
      map: this.map,
    });
    
    this.listener = this.map.addListener('click', (e)=> {
      
      console.log('length', this.markArr.length);
      if (this.markArr.length >= 2) {
        return;
      } 

      let markerTmp = new google.maps.Marker({
        position: e.latLng,
        map: this.map,
        draggable: false
      });

      eleva.getElevationForLocations({locations: [e.latLng]}, (results, status)=> {
        if (google.maps.ElevationStatus.OK == status) {
          let infoWindow = new google.maps.InfoWindow();
          infoWindow.open(this.map, markerTmp);
          infoWindow.setContent('標高:' + results[0].elevation + ' メートル');
          this.infoWindowArr.push(infoWindow);
        } else {
          console.log('request Elevation がエラーになった');
        }
      });
      
      this.markArr.push(markerTmp);
      polyline.getPath().push(markerTmp.getPosition());
    });
    
    this.ploylineArr.push(polyline);
    this.listenerArr.push(this.listener);
  }

  async calculateSlope() {
    this.cleanInfoWindow();
    // Math.tan(x) * 180 / Math.PI
    let distance = google.maps.geometry.spherical.computeDistanceBetween(this.markArr[0].position, this.markArr[1].position);

    let eleva = new google.maps.ElevationService();
    let elevaA: any = await this.getEleva(0);
    let elevaB: any = await this.getEleva(1);

    // Math.tan(x) * 180 / Math.PI

    console.log('elevaA:', elevaA);
    console.log('elevaB:', elevaB);
    console.log('distance:', distance);
    let angle = Math.tan((Math.abs(elevaA - elevaB) / distance)) * 180 / Math.PI;
    let infoWindow = new google.maps.InfoWindow();
    infoWindow.open(this.map, this.markArr[0]);
    var contentString = '<div >'+
            '標高：'+ Math.abs(elevaA - elevaB) + ',距離：' + distance + '</br>' +
            '角度：'+ angle + '度' + 
            '</div>';

    infoWindow.setContent(contentString);
    this.infoWindowArr.push(infoWindow);
  }

  getEleva(idx) {
    return new Promise((resolve, reject)=>{
      let eleva = new google.maps.ElevationService();
      eleva.getElevationForLocations({locations: [this.markArr[idx].position]}, (results, status)=> {
        if (google.maps.ElevationStatus.OK == status) {
          resolve(results[0].elevation);  
        } else {
          console.log('request Elevation がエラーになった');
        }
      });
    });
  }

  public isVisable() {
    this.isDisplay = this.isDisplay == 'none' ? 'block' : 'none';
  }

}
