import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { P5_1Page } from './p5-1';

@NgModule({
  declarations: [
    P5_1Page,
  ],
  imports: [
    IonicPageModule.forChild(P5_1Page),
  ],
})
export class P5_1PageModule {}
