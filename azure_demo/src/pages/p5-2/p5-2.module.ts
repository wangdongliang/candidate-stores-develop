import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { P5_2Page } from './p5-2';

@NgModule({
  declarations: [
    P5_2Page,
  ],
  imports: [
    IonicPageModule.forChild(P5_2Page),
  ],
})
export class P5_2PageModule {}
