import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BlobServiceProvider } from '../../providers/blob-service/blob-service';
import { AuthProvider } from '../../providers/auth/auth';
import { AlertController } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';

import { ActionSheetController } from "ionic-angular";
import { File } from '@ionic-native/file'
import { ViewCommonProvider } from '../../providers/view-common/view-common';


/**
 * Generated class for the DetailImgPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-detail-img',
    templateUrl: 'detail-img.html',
})
export class DetailImgPage {
    private imgUrl;
    private imgName;
    private container = 'pocstores';
    private account = 'pocstores';
    private temp = '';
    private result;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public blobServiceProvider: BlobServiceProvider,
        public alertCtrl: AlertController,
        public auth: AuthProvider,
        private camera: Camera,
        private imagePicker: ImagePicker,
        public actionSheetCtrl: ActionSheetController,
        private file: File
    ) {
    }

    ionViewDidLoad() {
        console.log('data', this.navParams.data);
        this.imgUrl = this.navParams.data['imgUrl'];
        this.imgName = this.navParams.data['imgName'];
    }

    upload() {
        this.file.resolveLocalFilesystemUrl(this.imgUrl).then(function (entry) {
            if (entry.isFile) {
                entry.file(this.success.bind(this), this.fail);
            }
            else {
                console.log('it is not a file');
            }
        }.bind(this));
    }

    private async success(file) {
        console.log("File size: " + file.size);

        let token = await this.auth.getToken(this.auth.storageResource);
        if (token) {
            console.log('this.auth.accessToken====', token);
            if (token) {
                this.blobServiceProvider.uploadBlob('pocstores',
                    token, 'pocstores', this.imgName, file,
                    (err) => {
                        if (err) {
                            // Upload blob failed
                            console.error('画像ファイルアップロードは失敗しました。');
                            this.result = '画像ファイルアップロードは失敗しました。'
                        } else {
                            // Upload successfully
                            console.log('画像ファイルアップロードは成功しました。');
                            this.result = '画像ファイルアップロードは成功しました。'
                        }
                    });
            }
        } else {
            console.error('認証失敗');
        }

    }

    private fail(error) {
        alert("Unable to retrieve file properties: " + error.code);
    }

    async delete() {
        // this.accessToken = await this.storage.get('accessToken');
        let alert = ViewCommonProvider.showConfirmationAlert(
            this.alertCtrl,
            '削除しますか。',
            '',
            async () => {
                let token = await this.auth.getToken(this.auth.storageResource);
                if (token) {
                    console.log('this.auth.accessToken====', token);
                    if (token) {
                        this.blobServiceProvider.deleteBlob(this.account, token, this.container, this.imgName).then(() => {
                            let alert = this.alertCtrl.create({
                                title: 'confirm',
                                message: 'delete success!',
                            });
                            alert.present();
                            setTimeout(() => {
                                alert.dismiss();
                                this.navCtrl.pop();
                            }, 1000);
                        });
                    }
                } else {
                    console.error('認証失敗');
                }
            },
            ()=>{
                return;
            }
        );

    }

    private showPicActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            title: '選択してください。',
            buttons: [
                {
                    text: '撮ります',
                    handler: () => {
                        this.takePicture();
                    }

                },
                {
                    text: '写真から選択',
                    handler: () => {
                        this.openImgPicker();
                    }
                },
                {
                    text: '取り消し',
                    role: 'cancel',
                    handler: () => {
                    }
                }

            ]
        });
        actionSheet.present();
    }

    takePicture() {

        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        }

        this.camera.getPicture(options).then((imgUrl) => {
            this.imgUrl = imgUrl;
            let alert = this.alertCtrl.create({
                title: 'upload?',
                buttons: [
                    {
                        text: 'OK',
                        handler: () => {
                            this.upload();
                        }
                    },
                    {
                        text: 'Cancel',
                    },
                ]
            });
            alert.present();
        }, (err) => {
            // Handle error
            console.error('camera err' + err);

        });
    }

    // openImgPicker
    private openImgPicker() {
        this.imagePicker.getPictures(this.imagePickerOpt).then((results) => {
            for (var i = 0; i < results.length; i++) {
                this.imgUrl = results[i];
            }

            let alert = this.alertCtrl.create({
                title: 'upload?',
                buttons: [
                    {
                        text: 'OK',
                        handler: () => {
                            this.upload();
                        }
                    },
                    {
                        text: 'Cancel',
                    },
                ]
            });
            alert.present();
        }, (err) => {

        });
    }

    // 
    private imagePickerOpt = {
        maximumImagesCount: 1,//
        width: 800,
        height: 800,
        quality: 80
    };

}
