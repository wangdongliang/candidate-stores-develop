import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreImgPage } from './cre-img';

@NgModule({
  declarations: [
    CreImgPage,
  ],
  imports: [
    IonicPageModule.forChild(CreImgPage),
  ],
})
export class CreImgPageModule {}
