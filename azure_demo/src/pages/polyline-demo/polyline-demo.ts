import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the PolylineDemoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare let google;

@IonicPage()
@Component({
  selector: 'page-polyline-demo',
  templateUrl: 'polyline-demo.html',
})
export class PolylineDemoPage {

  //Googlemap表示オブジェクト
  @ViewChild('map') mapElement: ElementRef;
  private map: any;
  private propertyKey: string;
  private propertyVal: string;
  private properties = [];
  private polyLineArr = [];
  private listenerArr = [];
  private isDisplay = 'block';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.initMap();
  }
  
  private initMap() {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      center: {lat: 35.459471, lng: 139.627371},
      zoom: 15,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false
    });
  }


  private setPropertyValue() {
    console.log('propertyKey:', this.propertyKey);
    this.cleanAll();

    switch(this.propertyKey) {
      case 'basic':
        this.properties = [];
        this.setPloyLineWithBasic();
        break;
      case 'draggable':
        this.properties = [];
        this.properties = ['true', 'false'];
        break;
      case 'editable':
        this.properties = [];
        this.properties = ['true', 'false'];    
        break;
      case 'icons':
        this.properties = [];
        this.properties = [google.maps.SymbolPath.CIRCLE, 
             google.maps.SymbolPath.FORWARD_CLOSED_ARROW];
        break;
      case 'strokeColor':
        this.properties = [];
        this.properties = ['red', 'blue', 'green'];
        break;
      case 'strokeOpacity':
        this.properties = [];
        this.properties = ['0.5', '1'];
        break;
      case 'strokeWeight':
        this.properties = [];
        this.properties = ['2', '5', '8'];
        break;
    }
  }

  private setPloyLineWithBasic() {
    let polyline = new google.maps.Polyline({
      map: this.map
    });

    let listener = this.map.addListener('click', (e)=> {
      console.log('map click e:', e);
      console.log('lat():', e.latLng.lat());
      console.log('lng():', e.latLng.lng());

      polyline.getPath().push(e.latLng);
    });

    this.polyLineArr.push(polyline);
    this.listenerArr.push(listener);
  }

  public changePropertyVal() {
    console.log('propertyVal:', this.propertyVal);
    this.cleanAll();

    switch(this.propertyKey) {
      case 'draggable':
        this.setPloyLineWithDraggable();
        break;
      case 'editable':
        this.setPloyLineWithEditable();        
        break;
      case 'icons':
        this.setPloyLineWithIcons();
        break;
      case 'strokeColor':
        this.setPloyLineWithStrokeColor();
        break;
      case 'strokeOpacity':
        this.setPloyLineWithStrokeOpacity();
        break;
      case 'strokeWeight':
        this.setPloyLineWithStrokeWeight();
        break;
    }
  }

  private setPloyLineWithDraggable() {
    let polyline = new google.maps.Polyline({
      strokeColor: '#FF0000',
      strokeOpacity: '0.8',
      strokeWeight: '5',
      map: this.map,
      draggable: this.propertyVal === 'true'
    });

    let listener = this.map.addListener('click', (e)=> {
      polyline.getPath().push(e.latLng);
    });

    this.polyLineArr.push(polyline);
    this.listenerArr.push(listener);
  }

  private setPloyLineWithEditable() {
    let polyline = new google.maps.Polyline({
      strokeColor: '#FF0000',
      strokeOpacity: '0.8',
      strokeWeight: '5',
      map: this.map,
      editable: this.propertyVal === 'true'
    });

    let listener = this.map.addListener('click', (e)=> {
      polyline.getPath().push(e.latLng);
    });

    this.polyLineArr.push(polyline);
    this.listenerArr.push(listener);
  }

  private setPloyLineWithIcons() {

    var symbol = {
      path: this.propertyVal
    };

    let polyline = new google.maps.Polyline({
      strokeColor: '#FF0000',
      strokeOpacity: '0.8',
      strokeWeight: '5',
      map: this.map,
      icons: [{
        icon: symbol,
        offset: '100%'
      }]
    });

    let listener = this.map.addListener('click', (e)=> {
      polyline.getPath().push(e.latLng);
    });

    this.polyLineArr.push(polyline);
    this.listenerArr.push(listener);
  }

  private setPloyLineWithStrokeColor() {
    let polyline = new google.maps.Polyline({
      strokeColor: this.propertyVal,
      strokeOpacity: '0.8',
      strokeWeight: '5',
      map: this.map,
    });

    let listener = this.map.addListener('click', (e)=> {
      polyline.getPath().push(e.latLng);
    });

    this.polyLineArr.push(polyline);
    this.listenerArr.push(listener);
  }

  private setPloyLineWithStrokeOpacity() {
    let polyline = new google.maps.Polyline({
      strokeColor: '#FF0000',
      strokeOpacity: this.propertyVal,
      strokeWeight: '5',
      map: this.map,
    });

    let listener = this.map.addListener('click', (e)=> {
      polyline.getPath().push(e.latLng);
    });

    this.polyLineArr.push(polyline);
    this.listenerArr.push(listener);
  }

  private setPloyLineWithStrokeWeight() {
    let polyline = new google.maps.Polyline({
      strokeColor: '#FF0000',
      strokeOpacity: '0.8',
      strokeWeight: this.propertyVal,
      map: this.map,
    });

    let listener = this.map.addListener('click', (e)=> {
      polyline.getPath().push(e.latLng);
    });

    this.polyLineArr.push(polyline);
    this.listenerArr.push(listener);
  }

  private setDraggable() {
    this.polyLineArr.forEach(element => {
      element.setDraggable(!element.getDraggable())
    });
  }

  private setEditable() {
    this.polyLineArr.forEach(element => {
      element.setEditable(!element.getEditable())
    });
  }

  private setOptions() {
    var symbol = {
      path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
    };
    this.polyLineArr.forEach(element => {
      element.setOptions({
        strokeColor: 'blue',
        strokeOpacity: 0.8,
        strokeWeight: 15,
        map: this.map,
        icons: [{
          icon: symbol,
          offset: '100%'
        }]
      })
    });
  }

  public addTriggerToPoly() {
    this.cleanAll();
    let ploLine = new google.maps.Polyline({
      strokeColor: '#FF0000',
      strokeOpacity: '0.8',
      strokeWeight: '5',
      map: this.map,
      editable: true,
      draggable: true,
      path:[{lat: 35.46139353872545, lng: 139.6221782433472},
           {lat: 35.46083425947163, lng: 139.62870137567143},
          ]
    });

    ploLine.addListener('click', ()=> {
      let infoWindow = new google.maps.InfoWindow();
      infoWindow.setContent('click event');
      infoWindow.setPosition({lat: 35.46139353872545, lng: 139.6221782433472});
      infoWindow.open(this.map);
    });

    ploLine.addListener('dragend', ()=> {
      let infoWindow = new google.maps.InfoWindow();
      infoWindow.setContent('dragend event');
      infoWindow.setPosition({lat: 35.46139353872545, lng: 139.6221782433472});
      infoWindow.open(this.map);
    });

    this.polyLineArr.push(ploLine);
  }

  private cleanAll() {
    this.polyLineArr.forEach((element)=> {
      element.setMap(null);
    });
  }

  public isVisable() {
    this.isDisplay = this.isDisplay == 'none' ? 'block' : 'none';
  }

}
