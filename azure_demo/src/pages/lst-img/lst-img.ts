import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DetailImgPage } from '../detail-img/detail-img';
import { CreImgPage } from '../cre-img/cre-img';
import { BlobServiceProvider } from '../../providers/blob-service/blob-service';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the LstImgPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lst-img',
  templateUrl: 'lst-img.html',
})
export class LstImgPage {

  // picture List
  private imgList;

  // picture URL
  private imgUrl = undefined;

  // picture First
  private imgFirst;

  // picture Name
  private imgName;

  constructor(
    public navCtrl: NavController,
    public blobServiceProvider: BlobServiceProvider,
    public auth: AuthProvider,
    public navParams: NavParams,
    public LoadingController: LoadingController) {
  }

  async ionViewWillEnter() {
    let loading = this.LoadingController.create({
      content: 'Please wait...'
    });
    loading.present();
    this.imgFirst = '';
    this.imgUrl = '';

    let token = await this.auth.getToken(this.auth.storageResource);
    if (token) {
      console.log('this.auth.accessToken====', token);
      if (token) {
        this.blobServiceProvider.getBlobList('pocstores', token, 'pocstores',
          (rlt) => {
            if (rlt != undefined && rlt != null && rlt.length != 0) {
              this.imgList = rlt;
              console.log(this.imgList);
              this.imgFirst = this.imgList[0];
              this.imgChange(this.imgFirst);
            }
          }
        );
        loading.dismiss();
      }
    } else {
      console.error('認証失敗');
    }
  }

  async imgChange(imgName) {
    console.log(imgName);
    let token = await this.auth.getToken(this.auth.storageResource);
    if (token) {
      console.log(token);
      this.imgUrl = this.blobServiceProvider.downloadBlob('pocstores', token, 'pocstores', imgName, (url) => {
        this.imgUrl = url;
      });
      this.imgName = imgName;
    } else {
      console.error('認証失敗');
    }
  }

  goAdd() {
    this.navCtrl.push(CreImgPage);
  }

  goDetail() {
    let params = {
      imgUrl: this.imgUrl,
      imgName: this.imgName
    };
    this.navCtrl.push(DetailImgPage, params);
  }

}
