import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImgDemoListPage } from './img-demo-list';

@NgModule({
  declarations: [
    ImgDemoListPage,
  ],
  imports: [
    IonicPageModule.forChild(ImgDemoListPage),
  ],
})
export class ImgDemoListPageModule {}
