import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { ApiCommonProvider } from '../../providers/api-common/api-common'
import { ResponseContentType } from '@angular/http';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the MapImageDemoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare let google;
const host = "https://pocstoressample.azure-api.net/blob/images";

@IonicPage()
@Component({
  selector: 'page-map-image-demo',
  templateUrl: 'map-image-demo.html',
})

export class MapImageDemoPage {

  //Googlemap表示オブジェクト
  @ViewChild('map') mapElement: ElementRef;
  private map: any;
  private listener: any;
  private markArr = [];
  private imgUri;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private provider: ApiCommonProvider,
              private auth: AuthProvider,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController) {
  }
    
  ionViewDidLoad() {
    this.initMap();
  }
  
  private initMap() {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      center: {lat: 35.459471, lng: 139.627371},
      zoom: 12,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false,
      clickableIcons: false
    });
  }
  
  public selIcon(icon: any) {
    
    console.log('icon:', icon);
    if (this.listener) {
      console.log('remove');
      this.listener.remove();
    }
    this.listener = this.map.addListener('click', (e)=> {
      let markerTmp = new google.maps.Marker({
        position: e.latLng,
        map: this.map,
        draggable: true,
        // icon: {url: `assets/imgs/${icon}`, size: {width: 50, height: 50}}
        icon: {url: `assets/imgs/${icon}`}
      });

      this.markArr.push(markerTmp);
    });
  }

  // https://maps.googleapis.com/maps/api/staticmap?center=35.459471,139.627371&zoom=12&size=400x400&markers=icon:https://pocstores.blob.core.windows.net/icontest/star.png%7C35.459471,139.627371
  public async ceateImg() {
    let center = this.map.getCenter();
    let markers = this.getMarkString(this.markArr);

    let imgUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${center.lat()},${center.lng()}&zoom=12&size=400x400${markers}`;
    imgUrl = imgUrl + '&key=AIzaSyB_mNY6MXIveedihJndsdHCY0w3L1lgqYA';

    console.log('imgUrl:', imgUrl);
    this.getImage(imgUrl);
  }

  private getMarkString(marks: Array<any>) {
    let rlt = '';
    marks.forEach((e)=> {
      let pos = e.getPosition();
      let uris = e.getIcon().url.split('/');
      let iconNm = uris[uris.length -1];
      rlt = rlt + `&markers=icon:https://pocstores.blob.core.windows.net/icon/${iconNm}%7C${pos.lat()},${pos.lng()}`;
    });
    return rlt;
  }

  async getImage(imgUrl) {
    
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();

    let token = await this.auth.getToken(this.auth.apiResource);
    // let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjdfWnVmMXR2a3dMeFlhSFMzcTZsVWpVWUlHdyIsImtpZCI6IjdfWnVmMXR2a3dMeFlhSFMzcTZsVWpVWUlHdyJ9.eyJhdWQiOiJodHRwczovL21hbmFnZW1lbnQuY29yZS53aW5kb3dzLm5ldC8iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9iMDIyOGRlYi1lNDIxLTRiMTgtOTY2YS1mNWY3NGE2YjExY2MvIiwiaWF0IjoxNTMyOTI3MjM1LCJuYmYiOjE1MzI5MjcyMzUsImV4cCI6MTUzMjkzMTEzNSwiYWNyIjoiMSIsImFpbyI6IkFUUUF5LzhJQUFBQVQvd3RITDcrUWR6M2p6a00ySjNxa0doeURBUlFkZWhrazhPYnZFQXlvdVVsTDBId3JuekRCZ1JTZzRLcTFhT0wiLCJhbHRzZWNpZCI6IjU6OjEwMDMzRkZGQUMzNDUzN0QiLCJhbXIiOlsicHdkIl0sImFwcGlkIjoiZDY3YjMwOWMtYTBmMy00NzA1LWJjYTUtMTIxMmM1M2JjN2FiIiwiYXBwaWRhY3IiOiIwIiwiZV9leHAiOjI2MjgwMCwiZW1haWwiOiJzdW56ZDExMTQwNkBkaGMuY29tLmNuIiwiZ3JvdXBzIjpbIjAzNTQ5YWYwLTJmMGYtNGIwZC1hOGU0LWI5NDBhZjQ4YTZmZiIsImM1YjBlODNkLWU0ODUtNDIwYi1hZDQ4LTE0ZjdlYWMxOTM5MyIsImE2MzhhZmNiLTUwODItNDViMS05MTlhLWRiMGU0OTFmMTI4ZCIsIjcxYmE1MDIwLTc4ZWUtNGNhZC1hMmY2LTViYmUyNmM2Y2JmZCIsIjZkYzU2Y2M4LTc4MDktNDAwNS1hOTMyLTNmMTBmNjM5MzEwNiJdLCJpZHAiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC8zNzU4MzBhZC02MzM3LTRjMjUtYmE5MS03M2RmZTJhZTI0NzIvIiwiaXBhZGRyIjoiNTIuMjQzLjQxLjM5IiwibmFtZSI6InN1bnpkMTExNDA2Iiwib2lkIjoiZTQ4M2VlMmQtODFhOS00ZmVhLTg2MGEtZThkMjBmN2U2M2ViIiwicHVpZCI6IjEwMDMzRkZGQUMyRkE4Q0MiLCJzY3AiOiJ1c2VyX2ltcGVyc29uYXRpb24iLCJzdWIiOiJoWEJTVUVjNHUxQnQ0SUEtYnAzamptMWstNVQ4dUdnbW1lYkJJN0NHMGF3IiwidGlkIjoiYjAyMjhkZWItZTQyMS00YjE4LTk2NmEtZjVmNzRhNmIxMWNjIiwidW5pcXVlX25hbWUiOiJzdW56ZDExMTQwNkBkaGMuY29tLmNuIiwidXRpIjoibVo0bEt1dkcza0dxMTl5TnZHQVZBQSIsInZlciI6IjEuMCIsIndpZHMiOlsiY2YxYzM4ZTUtMzYyMS00MDA0LWE3Y2ItODc5NjI0ZGNlZDdjIl19.s-z1tFhtw29vpf0959VFEj0b2q1Qd8UQJnEDRchgTMSIL6G5TQfKtISXF32BMLwCxo84JGWZ0BJz2LZIyjESfBsXNa5NGGI4nXNqcV7P2mRRP7_Q5qApAYMpUNnS9_bVqavOwUpOzs1eljN9OAovlSS9GGgRnugLIi820ojEedyjgxID9UBtutBCaA066xOvxsI_ulHMXKI6I6whFPV4AXlcXnB7_An1b8cUjv-M-PawEwVNDqftj5f60oUd4RK8mVkbf9vOmGVUU_WIr1JIatKlDJ498q2zhRVSFwH2S9udlfsmZYLmcMcPuEFqHq0iOmIfmbC9v5GsxZzISV5Lgg';
    let res: Response = await this.provider.doGetWithHeader(imgUrl, {'Cache-Control': 'no-cache, no-store, must-revalidate', 'content-type': 'image/png'}, ResponseContentType.Blob);
    let blob: Blob = await res.blob();

    let formData = new FormData();
    formData.append('file', blob, 'businessCenter.jpg');
    res = await this.provider.doPostWithHeader(`${host}?container=pocstores&type=create`, formData,  {'Authorization': `Bearer ${token}`});
    loading.dismiss();

    let json = await res.json();
    let resultMessage = json.message || json;
    this.showAlert(resultMessage);
  }
  

  showAlert(msg) {
    const alert = this.alertCtrl.create({
      title: '商圏',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

}

