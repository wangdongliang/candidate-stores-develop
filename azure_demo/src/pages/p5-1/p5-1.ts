import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import * as p5 from 'p5/lib/p5.min';
// import '../p5/lib/p5.min'
// import '../p5/lib/addons/p5.dom.min'
/**
 * Generated class for the P5_1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var p5: any;


@IonicPage()
@Component({
  selector: 'page-p5-1',
  templateUrl: 'p5-1.html',
})
export class P5_1Page {
  strokeWidth: number;  // ペンの太さRangeパーツで値が変動する
  drawFlag = false;
  myp5: any;
  x: number;
  y: number;
  w: number;
  h: number;
  r: number;
  dragFlag = false;
  cnv: any;
  pObj: any;
  inputObj: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.x = 250 / 2;
    this.y = 180 / 2;
    this.w = 50;
    this.h = 50;
    this.r = 40;
    console.log("Draw Picture");

  }

  dragPicture(param, drag?) {
    if (drag == 'drag') {
      this.dragFlag = true;
    } else {
      this.dragFlag = false;
    }
    let p5obj = (p) => {
      let dragging = false; // Is the object being dragged?
      let rollover = false; // Is the mouse over the ellipse?
      p.setup = (() => {
        if (this.pObj != undefined) {
          this.pObj.remove();
        }
        this.pObj = p;
        this.cnv = p.createCanvas(250, 180);
        // p.noLoop();
        // id="myCanvas"と紐づけ
        this.cnv.parent('myCanvas');
        p.textAlign(p.CENTER);
        p.textSize(20);
        // p.noLoop();
        if (param == 'text') {

          this.inputObj = p.createInput();
          this.inputObj.position(125, 90);
        }
      });
      p.draw = () => {
        p.background(210);
        let x = (p.width - 125) / 2 + 125 / 2;
        let y = (p.height - 90) / 2 + 90 / 2;
        if (p.mouseX > this.x && p.mouseX < this.x + this.w && p.mouseY > this.y && p.mouseY < this.y + this.h) {
          rollover = true;
        }
        else {
          rollover = false;
        }

        // Adjust location if being dragged
        if (dragging) {
          this.x = p.mouseX + p.offsetX;
          this.y = p.mouseY + p.offsetY;
        }

        p.stroke(0);
        // Different fill based on state
        if (dragging) {
          p.fill(50);
        } else if (rollover) {
          p.fill(100);
        } else {
          p.fill(175, 200);
        }
        switch (param) {
          case "rect":
            // p.rectMode(p.CENTER);
            // p.rectMode(p.CENTER);
            // p.translate(x, y);
            // p.rotate(this.rotateCompute(p));
            // this.x = x;
            // this.y = y;
            p.rect(this.x, this.y, this.w, this.h);
            // p.text('This is a rect', 90, 40);
            break;
          case "triangle":
            // p.text('This is a triangle', 90, 40);
            p.translate(p.width / 2, p.height / 2);
            let a = p.atan2(p.mouseY - p.height / 2, p.mouseX - p.width / 2);
            p.rotate(a);
            p.triangle(30, 75, 58, 20, 86, 75);
            break;
          case "ellipse":
            p.text('This is a ellipse', 90, 40);
            this.x = 160;
            this.y = 120;
            p.ellipse(this.x, this.y, this.r, this.r);
            break;
          case "line":
            p.text('This is a line', 90, 40);
            this.x = 100;
            this.y = 150;
            let x1 = p.mouseX;
            let y1 = p.mouseY;
            p.line(this.x, this.y, x1, y1);
            p.ellipse(this.x, this.y, 7, 7);
            p.ellipse(x1, y1, 7, 7);
            let distance = p.int(p.dist(this.x, this.y, x1, y1));
            p.push();
            p.translate((this.x + x1) / 2, (this.y + y1) / 2);
            p.rotate(p.atan2(y1 - this.y, x1 - this.x));
            p.text(p.nfc(distance, 1), 0, -5);
            p.pop();
            break;
          case "text":
            this.x = p.width / 2;
            this.y = p.height / 2;
            this.w = 50;
            this.h = 50;
            p.rectMode(p.CENTER);
            p.rect(this.x, this.y, this.w, this.h);
            // p.text('This is a rect', 90, 40);
            break;

        }

      }

      p.mousePressed = () => {
        // p.redraw();
        if (this.dragFlag) {
          // Did I click on the rectangle?
          // console.log(p.abs(p.mouseX) > this.x);
          // console.log(p.mouseX < this.x);
          // console.log(this.w && p.mouseY > this.y);
          // console.log(p.mouseY < this.y + this.h);
          if (p.abs(p.mouseX) > this.x && p.mouseX < this.x + this.w && p.mouseY > this.y && p.mouseY < this.y + this.h) {
            console.log("mousePressed");
            p.offsetX = this.x - p.mouseX;
            p.offsetY = this.y - p.mouseY;
            dragging = true;
          }
          // If so, keep track of relative location of click to corner of rectangle

        }
      }
      p.mouseReleased = () => {
        if (this.dragFlag) {
          dragging = false;
        }
      }
      p.mouseDragged = () => {
        if (dragging) {
          console.log("drag");
          this.x = p.mouseX + p.offsetX;
          this.y = p.mouseY + p.offsetY;
        }
      }
      // p.touchMoved = () => {
      //   console.log("kkkkkkkkkkkkkkk");
      //   // ペンの太さ
      //   p.strokeWeight(5);
      //   // ペンの色
      //   p.stroke('yellow');
      //   // なぞったエリアに線を描画
      //   // p.line(p.mouseX, p.mouseY, p.pmouseX, p.pmouseY);
      //   // p.rect(10, 5, 50, 50);

      //   p.rect(p.mouseX, p.mouseY, p.pmouseX, p.pmouseY);
      //   return false;
      // };
      // p.mouseIsPressed = () => {
      //   if (p.dist(p.mouseX, p.mouseY, this.x, this.y) < this.r) {
      //     this.x = p.mouseX;
      //     this.y = p.mouseY;
      //   }
      //   p.noLoop();
      // }
      // p.mouseWheel = (event) => {
      //   this.y += event.delta; // 円の位置を変更する
      //   p.ellipse(this.x, this.y, 20, 20);　// 円を描画する
      // }
    }
    // インスタンスモード
    this.myp5 = new p5(p5obj);
  }

  //回転角度の計算
  rotateCompute(pobj) {
    return pobj.atan2(pobj.mouseY - pobj.height / 2, pobj.mouseX - pobj.width / 2);
  }

  //リセット
  reset() {
    if (this.pObj != undefined) {
      this.pObj.remove();
    }
  }

}
