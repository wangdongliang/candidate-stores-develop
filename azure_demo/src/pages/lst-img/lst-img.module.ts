import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LstImgPage } from './lst-img';

@NgModule({
  declarations: [
    LstImgPage,
  ],
  imports: [
    IonicPageModule.forChild(LstImgPage),
  ],
})
export class LstImgPageModule {}
