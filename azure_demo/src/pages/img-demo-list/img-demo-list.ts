import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { ApiCommonProvider } from '../../providers/api-common/api-common'
import { Component, ChangeDetectorRef } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AuthProvider } from '../../providers/auth/auth';
import { ResponseContentType } from '@angular/http';
import { File } from '@ionic-native/file';

/**
 * Generated class for the ImgDemoListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
// const host = "https://imageuldltest.azurewebsites.net/api/ImguldlWithCheckFileName";
// const host = "https://pocstoressample2.azure-api.net/blob/images";
const host = "https://pocstoressample.azure-api.net/blob/images";

@IonicPage()
@Component({
  selector: 'page-img-demo-list',
  templateUrl: 'img-demo-list.html',
})

export class ImgDemoListPage {

  private img: any;
  private imgList = [];
  private imgUri: any;
  private resultMessage;
  private containerNm: any;

  constructor(private navCtrl: NavController, 
              private loadingCtrl: LoadingController,
              private camera: Camera,
              private changeDetectorRef: ChangeDetectorRef,
              private file: File,
              private provider: ApiCommonProvider,
              private auth: AuthProvider) {
              this.containerNm = 'pocstores';
  }

  ionViewDidEnter() {
    this.resultMessage = '';
    this.getImgList();
  }

  async getImageByName(img: any) {
    
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();

    let token = await this.auth.getToken(this.auth.apiResource);
    let res: Response = await this.provider.doGetWithHeader(host + `?name=${img.fileNm}&container=` + this.containerNm, {'Authorization': `Bearer ${token}`}, ResponseContentType.Blob);
    let blob: Blob = await res.blob();
    let reader = new FileReader();
    reader.readAsDataURL(blob); 
    reader.onloadend = ()=> {
      this.imgUri = reader.result;
      loading.dismiss();
    }
  }

  async getImgList() {
    let token = await this.auth.getToken(this.auth.apiResource);
    let res: Response = await this.provider.doGetWithHeader(host + '?container=' + this.containerNm, {'Authorization': `Bearer ${token}`});
    let json = await res.json();
    if (!this.checkAuth(res)) {
      this.resultMessage = json.message || json;
      return;
    }

    this.imgList = json;
    if (this.imgList.length > 0) {
      this.img = this.imgList[this.imgList.length - 1];
      this.getImageByName(this.img);
    }
  }

  changeItem() {
    this.resultMessage = '';
    if (this.img) {
      this.getImageByName(this.img);
    }
  }

  async delImageByName() {
    // 画像クリア
    this.imgUri = '';
    this.resultMessage = '';

    let token = await this.auth.getToken(this.auth.apiResource);
    let res: Response = await this.provider.doDeleteWithHeader(host + `?name=${this.img.fileNm}&container=` + this.containerNm, {'Authorization': `Bearer ${token}`});

    let json = await res.json();
    if (!this.checkAuth(res)) {
      this.resultMessage = json.message || json;
      return;
    }

    this.imgList = this.removeName(this.imgList, this.img);
    if (this.imgList.length > 0) {
      this.img = this.imgList[this.imgList.length - 1];
      this.getImageByName(this.img);
    }
    this.resultMessage = json;
  }
  
  removeName(imgList: Array<any>, img): Array<any> {
    let result = [];
    imgList.forEach((e)=>{
      if (e.fileNm != img.fileNm) {
        result.push(e);
      }
    });
    return result;
  }

  async uploadImage(type: number) {
    this.resultMessage = '';
    let token = await this.auth.getToken(this.auth.apiResource);
    
    const options: CameraOptions = {
      quality: 40,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.PNG,
      sourceType: type,
      allowEdit: true
    }
    
    let imageData = await this.camera.getPicture(options);
    let fileEntry = await this.file.resolveLocalFilesystemUrl(imageData);
    this.readBinaryFile(fileEntry, imageData, token);
  }

  readBinaryFile(fileEntry, imageData, token) {
    fileEntry.file((file)=> {
        var reader = new FileReader();
        reader.onloadend = async ()=> {
            let blob = new Blob([new Uint8Array(reader.result)], { type: 'image/png' });
            let fileName = this.createPicName();
            let formData = new FormData();
            formData.append('file', blob, fileName);
            
            let res: Response = await this.provider.doPostWithHeader(host + `?container=${this.containerNm}&type=create`, formData,  {'Authorization': `Bearer ${token}`});
            let json = await res.json();
            console.log('res:', res);
            console.log('json:', json);

            if (!this.checkAuth(res)) {
              this.resultMessage = json.message || json;
              this.changeDetectorRef.detectChanges();
              return;
            }

            let imgTmp = {fileNm: fileName, blobNm: 'pocstores'}
            this.imgList.push(imgTmp);
            this.img = imgTmp;
            this.imgUri = imageData;
            this.resultMessage = json;
            this.changeDetectorRef.detectChanges();
        };
        reader.readAsArrayBuffer(file);
    }, (error)=>{
      console.log(error);
    });
  }

  async updateImg() {
    // 画像クリア
    this.imgUri = '';
    this.resultMessage = '';
    let token = await this.auth.getToken(this.auth.apiResource);

    const options: CameraOptions = {
      quality: 40,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.PNG,
      sourceType: 1,
      allowEdit: true
    }
    
    let imageData = await this.camera.getPicture(options);
    let fileEntry: any = await this.file.resolveLocalFilesystemUrl(imageData);
    
    fileEntry.file((file)=> {
      var reader = new FileReader();
      reader.onloadend = async ()=> {
          let blob = new Blob([new Uint8Array(reader.result)], { type: 'image/png' });
          let fileName = this.img.fileNm;
          let formData = new FormData();
          formData.append('file', blob, fileName);
          
          let res = await this.provider.doPostWithHeader(host + `?container=${this.containerNm}&type=update`, formData,  {'Authorization': `Bearer ${token}`});
          let json = await res.json();

          console.log('res:', res);
          if (!this.checkAuth(res)) {
            this.resultMessage = json.message || json;
            this.changeDetectorRef.detectChanges();
            return;
          }

          this.imgUri = imageData;
          this.resultMessage = json;
          this.changeDetectorRef.detectChanges();
      };
      reader.readAsArrayBuffer(file);
    }, (error)=>{
      console.log(error);
    });
  }

  private createPicName() {
    let date = new Date();
    let m = date.getMonth() + 1
    let d = date.getDate();
    let h = date.getHours();
    let f = date.getMinutes();
    let s = date.getSeconds();
    let str = '' + date.getFullYear() + m + d + h + f + s;

    return str.concat('.png')
  }

  private checkAuth(res: Response): boolean {
    let checkResult = false;
    if (res.status == 200) {
      checkResult = true; 
    }

    return checkResult;
  }

  compareFn(e1: any, e2: any): boolean {
    return e1 && e2 ? e1.fileNm === e2.fileNm : e1 === e2;
  }

  public changeContainer() {
    this.resultMessage = '';
    this.imgList = [];
    this.imgUri  = '';
    this.getImgList();
  }

}

// fetchのリクエスト例：
// getImageByName(name) {
//   let loading = this.loadingCtrl.create({
//     content: 'Please wait...'
//   });
//   loading.present();

//   fetch(host + 'GetImageByName?name=' + name)
//     .then((response) => response.blob())
//     .then((blob) => {
//       let reader = new FileReader();
//       reader.readAsDataURL(blob); 
//       reader.onloadend = ()=> {
//         this.imgUri = reader.result;
//         loading.dismiss();
//       }
//   });
// }

// getImgList() {
//   fetch(host + "GetImageList")
//     .then((response) => response.json())
//     .then((res) => {
//       this.imgNms = res;
//       if (this.imgNms.length > 0) {
//         this.imgNm = this.imgNms[this.imgNms.length - 1];
//         this.getImageByName(this.imgNm);
//       }
//       console.log('text:', this.imgNms);
//   });
// }

// delImageByName() {
//   // 画像クリア
//   this.imgUri = "";
//   this.resultMessage = '';

//   fetch(host + "DelImageByName?name=" + this.imgNm)
//     .then((response) => response.text())
//     .then((text) => {
      
//       this.imgNms = this.removeName(this.imgNms, this.imgNm);
//       if (this.imgNms.length > 0) {
//         this.imgNm = this.imgNms[this.imgNms.length - 1];
//         this.getImageByName(this.imgNm);
//       }
//       this.resultMessage = text;
//       console.log('text:', text);
//   });
// }


// async uploadImage() {
//   this.resultMessage = '';

//   const options: CameraOptions = {
//     quality: 40,
//     destinationType: this.camera.DestinationType.FILE_URI,
//     encodingType: this.camera.EncodingType.PNG,
//     sourceType: this.camera.PictureSourceType.CAMERA,
//     allowEdit: true
//   }
  
//   let imageData = await this.camera.getPicture(options);
//   let fileEntry = await this.file.resolveLocalFilesystemUrl(imageData);
//   this.readBinaryFile(fileEntry, imageData);
// }

// readBinaryFile(fileEntry, imageData) {
//   fileEntry.file((file)=> {

//       console.log('name:', file.name);
//       console.log('name:', file.size);

//       var reader = new FileReader();
//       reader.onloadend = ()=> {
//           let blob = new Blob([new Uint8Array(reader.result)], { type: "image/png" });
//           let fileName = this.createPicName();
//           let formData = new FormData();
//           formData.append('file', blob, fileName);
          
//           fetch(host + 'UploadImage', {
//             method: 'POST',
//             body: formData	
//           })
//           .then(response => response.json())
//           .then(res=> {
//             this.imgNm = fileName;
//             this.imgNms.push(fileName);
//             this.imgUri = imageData;
//             this.resultMessage = res;
//             this.changeDetectorRef.detectChanges();
//           })
//       };
//       reader.readAsArrayBuffer(file);
//   }, (error)=>{
//     console.log(error);
//   });
// }
