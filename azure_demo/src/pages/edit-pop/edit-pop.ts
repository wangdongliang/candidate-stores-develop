import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ViewCommonProvider } from '../../providers/view-common/view-common';
import { ApimanagementServiceProvider } from '../../providers/apimanagement-service/apimanagement-service';

/**
 * Generated class for the EditPopPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-pop',
  templateUrl: 'edit-pop.html',
})
export class EditPopPage {

  // getDate
  private getData;

  // id
  private getStudentId;

  // studentName
  private getStudentName;

  // teacherId
  private getTeacherId;

  // email
  private getEmail;

  // pageFlg
  private pageFlg;

  // teacherList
  private teacherList;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alert: AlertController,
    public loading: LoadingController,
    private apimanagementService: ApimanagementServiceProvider) {
    this.getData = this.navParams.data;
  }

  ionViewDidLoad() {
    if (this.getData['Flg'] == 'editPage') {
      this.getStudentId = this.getData['data']['studentid'];
      this.getStudentName = this.getData['data']['studentname'];
      this.getTeacherId = this.getData['data']['teacherid'];
      this.getEmail = this.getData['data']['email'];
      this.pageFlg = true;
    } else {
      this.getStudentId = '';
      this.getStudentName = '';
      this.getEmail = '';
      this.getTeacherId = '';
      this.pageFlg = false;
    }
    this.teacherList = [
      {
        teacherId: '1',
        teacherName: 'teacherName1'
      },
      {
        teacherId: '2',
        teacherName: 'teacherName2'
      },
      {
        teacherId: '3',
        teacherName: 'teacherName3'
      }
    ]
  }

  private getStudentIdChange(studentId) {
    this.getStudentId = studentId;
  }

  private getStudentNameChange(studentName) {
    this.getStudentName = studentName;
  }

  private getTeacherIdChange(item) {
    this.getTeacherId = item.teacherId;
  }

  private getEmailChange(email) {
    this.getEmail = email;
  }

  private async commit() {
    let loading = this.loading.create({
      content: '更新中...'
    });
    loading.present();
    if (this.pageFlg) {
      let param = {
        studentId: this.getStudentId,
        studentName: this.getStudentName,
        teacherId: this.getTeacherId,
        email: this.getEmail
      };
      let res = await this.apimanagementService.editData(param);
      if (res['status'] == 200) {
        ViewCommonProvider.showErrorAlert(this.alert, '更新成功！');
      } else {
        if (res['status'] == '401') {
          ViewCommonProvider.showErrorAlert(this.alert, 'コード:401 このユーザーは更新権限がありません。');
        }
      }
      loading.dismiss();
    } else {
      let param = {
        studentId: '',
        studentName: this.getStudentName,
        teacherId: this.getTeacherId,
        email: this.getEmail
      };
      let res = await this.apimanagementService.createData(param);
      if (res['status'] == 200) {
        ViewCommonProvider.showErrorAlert(this.alert, '新規成功！');
      } else {
        if (res['status'] == '401') {
          ViewCommonProvider.showErrorAlert(this.alert, 'コード:401 このユーザーは新規権限がありません。');
        }
      }
      loading.dismiss();
    }
  }
}
