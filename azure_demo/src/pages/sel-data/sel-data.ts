import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, PopoverController } from 'ionic-angular';
import { ViewCommonProvider } from '../../providers/view-common/view-common';
import { ApimanagementServiceProvider } from '../../providers/apimanagement-service/apimanagement-service';

import { EditPopPage } from '../edit-pop/edit-pop'

/**
 * Generated class for the SelDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sel-data',
  templateUrl: 'sel-data.html',
})
export class SelDataPage {

  private firstFlg = true;
  // db data
  private demoData;

  private DBName = 'DB処理';

  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alert: AlertController,
    public popover: PopoverController,
    private apimanagementService: ApimanagementServiceProvider) {
  }

  ionViewWillEnter() {
  }

  private async selectButton() {
    console.log("aaaaaaaaaaaa");
    let loading = this.loading.create({
      content: '検索中...'
    });
    loading.present();
    let res = await this.apimanagementService.selectData();
    if (res['status'] == 200) {
      this.demoData = res.json();
      console.log('res_select:', res);
      if (this.firstFlg) {
        document.getElementById('selectPage').className = 'selectPage';
        this.firstFlg = false;
      }
    } else {
      if (res['status'] == '401') {
        ViewCommonProvider.showErrorAlert(this.alert, 'コード:401 このユーザーは検索権限がありません。');
      }
    }
    loading.dismiss();
    if (this.firstFlg) {
      document.getElementById('selectPage').className = 'selectPage';
      this.firstFlg = false;
    }
  }

  private createButton(id) {
    let param = {
      Flg: 'createPage'
    }
    let popover = this.popover.create(EditPopPage, param, {});
    popover.present();
    popover.onWillDismiss(() => {
      this.selectButton();
    });
  }

  private editButton(id, item) {
    let param = {
      Flg: 'editPage',
      id: id,
      data: item
    }
    let popover = this.popover.create(EditPopPage, param, {});
    popover.present();
    popover.onWillDismiss(() => {
      this.selectButton();
    });
  }

  private async deleteButton(studentId) {
    let loading = this.loading.create({
      content: '削除中...'
    })
    loading.present();
    let res = await this.apimanagementService.deleteData(studentId);
    if (res['status'] == 200) {
      ViewCommonProvider.showErrorAlert(this.alert, '削除成功！', () => { this.selectButton.bind(this.selectButton()) });
    } else {
      if (res['status'] == '401') {
        ViewCommonProvider.showErrorAlert(this.alert, 'コード:401 このユーザーは削除権限がありません。');
      }
    }
    loading.dismiss();
  }
}
