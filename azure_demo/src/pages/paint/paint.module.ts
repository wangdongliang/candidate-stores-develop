import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaintPage } from './paint';

@NgModule({
  declarations: [
    PaintPage,
  ],
  imports: [
    IonicPageModule.forChild(PaintPage),
  ],
})
export class PaintPageModule {}
