import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file';  

import { MyApp } from './app.component';
import { MenuPage } from '../pages/menu/menu';
import { LstImgPage } from '../pages/lst-img/lst-img';
import { DetailImgPage } from '../pages/detail-img/detail-img';
import { CreImgPage } from '../pages/cre-img/cre-img';
import { SelDataPage } from '../pages/sel-data/sel-data';
import { EditPopPage } from '../pages/edit-pop/edit-pop';
import { PdfViewerPage } from '../pages/pdf-viewer/pdf-viewer';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { BlobServiceProvider } from '../providers/blob-service/blob-service';
import { ApimanagementServiceProvider } from '../providers/apimanagement-service/apimanagement-service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ApiCommonProvider } from '../providers/api-common/api-common';
import { WelcomePage } from '../pages/welcome/welcome';
import { MSAdal } from '@ionic-native/ms-adal';

import { IonicStorageModule } from '@ionic/storage';
import { AuthProvider } from '../providers/auth/auth';
import { ViewCommonProvider } from '../providers/view-common/view-common';
import { SpeechRecognition } from '@ionic-native/speech-recognition';

@NgModule({
  declarations: [
    MyApp,
    MenuPage,
    LstImgPage,
    DetailImgPage,
    CreImgPage,
    SelDataPage,
    EditPopPage,
    WelcomePage,
    PdfViewerPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    PdfViewerModule,
    IonicStorageModule.forRoot()
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MenuPage,
    LstImgPage,
    DetailImgPage,
    CreImgPage,
    SelDataPage,
    EditPopPage,
    WelcomePage,
    PdfViewerPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    MSAdal,
    ImagePicker,
    File,
    SpeechRecognition,
    BlobServiceProvider,
    ApimanagementServiceProvider,
    ApiCommonProvider,
    AuthProvider,
    ViewCommonProvider,
  ]
})
export class AppModule {}
