import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LstImgPage } from './../lst-img/lst-img';

import { SelDataPage } from '../sel-data/sel-data';
/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var device;
declare var appAvailability;
declare var startApp;

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  goListImg() {
    this.navCtrl.push(LstImgPage);
  }

  goTest() {
    this.navCtrl.push(SelDataPage);
  }
  callxodo() {
    var scheme;
    // Don't forget to add the cordova-plugin-device plugin for `device.platform`
    if (device.platform === 'iOS') {
      scheme = 'iosamap://';
    }
    else if (device.platform === 'Android') {
      scheme = 'com.xodo.pdf.reader';
    }

    appAvailability.check(
      scheme,       // URI Scheme or Package Name
      function () {  // Success callback
        alert(scheme + ' は存在します :)');
        var sApp = startApp.set({ /* params */
          "application": scheme
        }, { /* extras */
            "EXTRA_STREAM": "extraValue1",
            "extraKey2": "extraValue2"
          });
        sApp.start(function () { /* success */
          alert("callxodo success");
        }, function (error) { /* fail */
          alert(error);
          console.log('error++++++', error);
        });

      },
      function () {  // Error callback
        alert(scheme + ' 存在しない :(');
      }
    );
  }

  navToMap() {
    this.navCtrl.push('MapDemoListPage');
  }

  navToImg() {
    this.navCtrl.push('ImgDemoListPage');
  }

  navToJs() {
    this.navCtrl.push('P5MenuPage');
  }
}
