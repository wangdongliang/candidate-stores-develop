import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PolygonDemoPage } from './polygon-demo';

@NgModule({
  declarations: [
    PolygonDemoPage,
  ],
  imports: [
    IonicPageModule.forChild(PolygonDemoPage),
  ],
})
export class PolygonDemoPageModule {}
