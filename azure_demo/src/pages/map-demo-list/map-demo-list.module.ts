import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapDemoListPage } from './map-demo-list';

@NgModule({
  declarations: [
    MapDemoListPage,
  ],
  imports: [
    IonicPageModule.forChild(MapDemoListPage),
  ],
})
export class MapDemoListPageModule {}
