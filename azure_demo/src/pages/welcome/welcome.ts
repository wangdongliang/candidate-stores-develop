import { Component } from '@angular/core';
import { IonicPage, NavController, Modal, ModalController, NavParams } from 'ionic-angular';
import { MenuPage } from './../menu/menu';
import { PdfViewerPage } from './../pdf-viewer/pdf-viewer';
import { AuthProvider } from '../../providers/auth/auth';
import { AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { ViewCommonProvider } from '../../providers/view-common/view-common';

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  private loginSuccessed = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public http: Http,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

  async goMenu() {

    // this.navCtrl.push(MenuPage);

    let token = await this.auth.getToken(this.auth.apiResource);
    console.log('token', token);
    if (token) {
      this.navCtrl.push(MenuPage);
    } else {
      ViewCommonProvider.showErrorAlert(this.alertCtrl, '認証失敗しました。');
    }
  }

  logout() {
    this.auth.clearCache();
    let logoutUrl = "https://login.windows.net/b0228deb-e421-4b18-966a-f5f74a6b11cc/oauth2/logout";
    this.http.get(logoutUrl).toPromise().then((res) => {
      console.log(res.status, res.statusText);
      if (res.status = 200) {
        ViewCommonProvider.showErrorAlert(this.alertCtrl, 'ログアウトしました。');
      }
    }).catch((e) => {
      console.error(e);
    });
  }

  openfile() {
    let modal: Modal = this.modalCtrl.create(PdfViewerPage);
    modal.present();
  }
}
