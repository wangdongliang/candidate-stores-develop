import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Shape, Point, Line } from '../../providers/shape/shape';

/**
 * Generated class for the PaintPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var p5: any;

@IonicPage()
@Component({
  selector: 'page-paint',
  templateUrl: 'paint.html',
})
export class PaintPage {
  myP5;
  ctx;
  canvas;
  selected;
  myShapes = [];
  dragoffx;
  dragoffy;
  color;
  scaleV;
  img;
  selection;
  valid = false;
  resizeDragging = false;
  expectResize = -1;
  selNumber = -1;
  targetLine = new Line(null, null);
  scale = 1;
  changedLable: any;
  scaleInput: any;
  scaleCurrenNum: any;
  scaleFlg = false;
  setOverFlg = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaintPage');
  }

  ionViewDidEnter() {
    let p5obj = (p) => {
      p.setup = (() => {
        let c = p.createCanvas(700, 700);
        c.position(90, 58);
        this.canvas = document.getElementById('defaultCanvas0');
        this.ctx = this.canvas.getContext('2d');

        p.background(200);

        // this.color = p.color(p.random(255), p.random(255), p.random(255), 128);
        // this.color = 'black';

        p.stroke(0);
        p.strokeWeight(2);

        let fileInput = p.createFileInput((file) => {
          if (file.type === 'image') {
            this.selected = 'image';
            this.img = p.loadImage(file.data, () => {
              p.background(this.img);
              this.valid = false;
            })

          }
        });

        fileInput.position(125, 5);
        // this.valid = false;
        let saveBtn = p.createButton('save');
        saveBtn.size(60, 20);
        saveBtn.position(125, 30);
        saveBtn.mousePressed(() => {
          this.targetLine = new Line(null, null);
          p.saveCanvas(this.canvas, 'test', 'jpg');
        });

        // let rotateBtn = p.createButton("回転");
        // rotateBtn.size(60, 30);
        // rotateBtn.position(5, 0 + 50);
        // rotateBtn.mousePressed(() => {
        //   this.selected = 'rotate';
        // });

        // let dragBtn = p.createButton("Drag");
        // dragBtn.size(60, 30);
        // dragBtn.position(5, 40 + 50);
        // dragBtn.mousePressed(() => {
        //   this.selected = 'drag';
        // });

        let scaleBtn = p.createButton("縮尺設定");
        scaleBtn.size(80, 30);
        scaleBtn.position(5, 60);
        scaleBtn.mousePressed(() => {
          if (this.scaleInput.value() != null && this.scaleInput.value() != '') {
            p.strokeWeight(2);
            // var c = p.color(255, 204, 0);
            // p.fill(c);
            p.stroke('blue');
            this.selected = 'scale';
            this.setOverFlg = false;
            if (this.scaleCurrenNum != undefined && this.scaleCurrenNum != null) {
              this.myShapes.splice(this.scaleCurrenNum, 1);
            }



            // let dis = p.int(p.dist(30, 35, 100, 35));
            // this.scale = Number((this.scaleInput.value() / dis).toFixed(2));
            // this.scaleFlg = true;
            // var lineShape = new Shape(p, this.color, this.selected, 30, 35, null, null, 100, 35, this.scale, this.scaleFlg, this.scaleInput.value());
            // this.myShapes.push(lineShape);
            // this.scaleCurrenNum = this.myShapes.length - 1;
            // this.targetLine = new Line(null, null);
            // this.valid = false;
          }
        });
        let mLable = p.createP('m');
        mLable.position(75, 60 + 40);
        this.scaleInput = p.createInput('');
        this.scaleInput.style('width', '70px');
        this.scaleInput.position(5, 60 + 40);

        this.scaleInput.changed(() => {

        })

        let colorBtn = p.createButton("色変更");
        colorBtn.size(80, 30);
        colorBtn.position(5, 60 + 40 * 3);

        let radio = p.createRadio();
        radio.size(80, 30);
        radio.position(5, 60 + 40 * 4);
        radio.option('black');
        radio.option('blue');
        radio.option('yellow');
        radio.option('red');
        radio.style('width', '45px');

        colorBtn.mousePressed(() => {
          if (radio.value() == undefined || radio.value() == null) {
            this.selected = 'color';
            this.color = 'black';
          } else {
            this.color = radio.value();
          }
          this.targetLine = new Line(null, null);
        });

        // p.textAlign(p.CENTER);

        let lineBtn = p.createButton("直線");
        lineBtn.size(80, 30);
        lineBtn.position(5, 60 + 40 * 6);
        lineBtn.mousePressed(() => {
          this.selected = 'line';
          this.targetLine = new Line(null, null);
        });

        let triangleBtn = p.createButton("三角型");
        triangleBtn.size(80, 30);
        triangleBtn.position(5, 60 + 40 * 7);
        triangleBtn.mousePressed(() => {
          this.selected = 'triangle';
          this.targetLine = new Line(null, null);
        });

        let resizeBtn = p.createButton("修正");
        resizeBtn.size(80, 30);
        resizeBtn.position(5, 60 + 40 * 8);
        resizeBtn.mousePressed(() => {
          this.selected = 'resize';
          this.targetLine = new Line(null, null);
        });

        let delBtn = p.createButton("削除");
        delBtn.size(80, 30);
        delBtn.position(5, 60 + 40 * 9);
        delBtn.mousePressed(() => {
          this.selected = 'delete';
          this.targetLine = new Line(null, null);
          console.log(this.myShapes.length);
          if (this.selection !== undefined && this.selection !== null) {
            console.log(this.selNumber);
            this.myShapes.splice(this.selNumber, 1);
            console.log(this.myShapes.length);
            this.valid = false;
          } else {
            alert("削除したい図形を選択してください。");
          }
        });

        // let clearBtn = p.createButton("clear");
        // clearBtn.size(80, 30);
        // clearBtn.position(5, 160 + 50);
        // clearBtn.mousePressed(() => {
        //   p.createCanvas(700, 700);
        //   p.background(200);
        //   this.selected = 'clear';
        // });

        // let rectBtn = p.createButton("四角型");
        // rectBtn.size(60, 30);
        // rectBtn.position(5, 200 + 50);
        // rectBtn.mousePressed(() => {
        //   p.stroke(0);
        //   p.strokeWeight(1);
        //   this.selected = 'rect';
        // });

        // let loadImgBtn = p.createButton("画像Load");
        // loadImgBtn.size(60, 30);
        // loadImgBtn.position(5, 280 + 50);
        // loadImgBtn.mousePressed(() => {
        //   this.selected = 'loadImg';
        //   this.loadImg(p);
        // });



        // let ellipseBtn = p.createButton("円型");
        // ellipseBtn.size(60, 30);
        // ellipseBtn.position(5, 280 + 50);
        // ellipseBtn.mousePressed(() => {
        //   p.stroke(0);
        //   p.strokeWeight(1);
        //   this.selected = 'ellipse';
        // });

        // let textBtn = p.createButton("Text");
        // textBtn.size(60, 30);
        // textBtn.position(5, 320 + 50);
        // textBtn.mousePressed(() => {
        //   p.stroke(0);
        //   p.strokeWeight(1);
        //   this.selected = 'text';
        // });



      });

      p.draw = (() => {
        if (!this.valid) {
          if (this.img != undefined && this.img != null) {
            let c = p.createCanvas(this.img.width, this.img.height);
            p.background(this.img);
          }
          // this.clearCtx(p);
          for (let shape of this.myShapes) {
            // if shape.
            shape.scale = this.scale;
            shape.draw(this.ctx, this.color, p);
          }
          if (this.changedLable != undefined && this.changedLable != null) {
            if (this.scaleInput.value() != null && this.scaleInput.value() != '') {
              p.strokeWeight(2);
              p.stroke('blue');
              p.line(30, 35, 80, 35);
              this.changedLable.remove();
              this.changedLable = p.createP('');
              this.changedLable.position(130, 60);
              this.changedLable.html(this.scaleInput.value() + 'm', false);
            } else {
              this.scale = 1;
            }
          }
          this.valid = true;
        }
      });

      // p.doubleClicked = (() => {
      //   if (p.mouseX > p.width || p.mouseX < 0 || p.mouseY > p.height || p.mouseY < 0) {
      //     return;
      //   }
      //   console.log('doubleClicked', this.selected);
      //   console.log(p.mouseX, p.mouseY);
      //   if (this.selected === 'rect') {
      //     var rectShape = new Shape(p, this.color, this.selected, p.mouseX, p.mouseY, 50, 50);
      //     this.myShapes.push(rectShape);
      //     let selectionHandles = [];
      //     for (let i = 0; i < 8; i += 1) {
      //       selectionHandles.push(rectShape);
      //     }
      //     rectShape.draw(this.ctx, this.color, p);
      //   } else if (this.selected === 'line') {
      //     if (this.targetLine.sPoint == null) {
      //       this.targetLine.sPoint = new Point(p.mouseX, p.mouseY);
      //     } else {
      //       this.targetLine.ePoint = new Point(p.mouseX, p.mouseY);
      //       var lineShape = new Shape(p, this.color, this.selected, this.targetLine.sPoint.x, this.targetLine.sPoint.y, null, null, this.targetLine.ePoint.x, this.targetLine.ePoint.y, this.scale);
      //       this.myShapes.push(lineShape);
      //       this.targetLine = new Line(null, null);
      //       this.valid = false;
      //     }
      //   } else if (this.selected === 'triangle') {
      //     var triangleShape = new Shape(p, this.color, this.selected, p.mouseX, p.mouseY - 50);
      //     this.myShapes.push(triangleShape);
      //     this.valid = false;
      //   } else if (this.selected === 'ellipse') {
      //     var ellipseShape = new Shape(p, this.color, this.selected, p.mouseX, p.mouseY, 50, 50);
      //     this.myShapes.push(ellipseShape);
      //     this.valid = false;
      //   } else if (this.selected === 'text') {
      //     p.textSize(24);
      //     var textShape = new Shape(p, this.color, this.selected, p.mouseX, p.mouseY);
      //     this.myShapes.push(textShape);
      //     this.valid = false;
      //   } else if (this.selected === 'clear') {
      //     this.myShapes = [];
      //     this.clearCtx(p);
      //   }
      //   return;
      // });

      p.mouseClicked = (() => {
        if (p.mouseX > p.width || p.mouseX < 0 || p.mouseY > p.height || p.mouseY < 0) {
          return;
        }
        console.log("mouseClicked");
        if (this.selected === 'triangle') {
          if (this.targetLine.sPoint == null) {
            this.targetLine.sPoint = new Point(p.mouseX, p.mouseY);
          } else {
            this.targetLine.ePoint = new Point(p.mouseX, p.mouseY);
            var triangleShape = new Shape(p, this.color, this.selected, this.targetLine.sPoint.x, this.targetLine.sPoint.y, 0, 0, this.targetLine.ePoint.x, this.targetLine.ePoint.y, this.scale);

            this.myShapes.push(triangleShape);
            this.valid = false;
            this.targetLine = new Line(null, null);
          }
        } else if (this.selected === 'line') {
          if (this.targetLine.sPoint == null) {
            this.targetLine.sPoint = new Point(p.mouseX, p.mouseY);
          } else {
            this.targetLine.ePoint = new Point(p.mouseX, p.mouseY);
            var lineShape = new Shape(p, this.color, this.selected, this.targetLine.sPoint.x, this.targetLine.sPoint.y, null, null, this.targetLine.ePoint.x, this.targetLine.ePoint.y, this.scale);
            this.myShapes.push(lineShape);
            this.targetLine = new Line(null, null);
            this.valid = false;
          }
        } else if (this.selected === 'scale') {
          if (this.setOverFlg) {
            return;
          }
          if (this.targetLine.sPoint == null) {
            this.targetLine.sPoint = new Point(p.mouseX, p.mouseY);
          } else {
            this.targetLine.ePoint = new Point(p.mouseX, p.mouseY);
            let dis = p.int(p.dist(this.targetLine.sPoint.x, this.targetLine.sPoint.y, this.targetLine.ePoint.x, this.targetLine.ePoint.y));
            this.scale = Number((this.scaleInput.value() / dis).toFixed(2));
            this.scaleFlg = true;
            // var lineShape = new Shape(p, this.color, this.selected, this.targetLine.sPoint.x, this.targetLine.sPoint.y, null, null, this.targetLine.ePoint.x, this.targetLine.ePoint.y, this.scale);
            var lineShape = new Shape(p, this.color, this.selected, this.targetLine.sPoint.x, this.targetLine.sPoint.y, null, null, this.targetLine.ePoint.x, this.targetLine.ePoint.y, this.scale, this.scaleFlg, this.scaleInput.value());
            console.log("lineShape:!!!!!!!!!!!!!", lineShape);
            this.myShapes.push(lineShape);
            this.scaleCurrenNum = this.myShapes.length - 1;
            this.targetLine = new Line(null, null);
            this.valid = false;
            this.setOverFlg = true;
          }
        }
      });

      p.mousePressed = (() => {
        if (p.mouseX > p.width || p.mouseX < 0 || p.mouseY > p.height || p.mouseY < 0) {
          return;
        }

        if (this.expectResize !== -1) {
          this.resizeDragging = true;
          return;
        }

        console.log('mousePressed');
        console.log('myShapes.length====', this.myShapes.length);
        for (var i = this.myShapes.length - 1; i >= 0; i--) {
          var mySel = this.myShapes[i];
          var selNumber0 = i;
          this.selNumber = selNumber0;
          if (mySel.pressed(p)) {
            for (var s of this.myShapes) {
              s.selectedFlg = false;
            }
            mySel.selectedFlg = true;
            this.selection = mySel;
            this.valid = false;
            // p.fill(155, 155, 0, 128);
            return;
          }
        }

        if (this.selection && this.selected != 'rotate') {
          this.selection.selectedFlg = false;
          this.selection = null;
          this.valid = false; // Need to clear the old selection border
        }
      });

      p.mouseDragged = (() => {
        if (p.mouseX > p.width || p.mouseX < 0 || p.mouseY > p.height || p.mouseY < 0) {
          return;
        }
        console.log('mouseDragged', this.selected);
        if (this.selected === 'rotate') {
          for (let s of this.myShapes) {
            if (s.selectedFlg) {
              this.valid = false;
              s.rotate(p);
              return;
            }
          }
        } else if (this.selected === 'drag') {
          if (this.selection !== undefined && this.selection !== null) {
            switch (this.selection.type) {
              case 'rect':
              case 'ellipse':
              case 'text':
              case 'triangle':
                if (p.mouseX > p.width) {
                  p.mouseX = p.width;
                } else if (p.mouseX < 0) {
                  p.mouseX = 0;
                }
                if (p.mouseY > p.height) {
                  p.mouseY = p.height;
                } else if (p.mouseY < 0) {
                  p.mouseY = 0;
                }

                this.selection.drag(p);
                break;
              default:
                break;
            }
          }
          this.valid = false; // Something's dragging so we must redraw
        } else if (this.selected === 'resize') {
          if (this.resizeDragging) {
            if (this.selection !== undefined && this.selection !== null) {
              switch (this.selection.type) {
                case 'rect':
                  //RECT
                  let oldx = this.selection.x;
                  let oldy = this.selection.y;

                  // 0  1  2
                  // 3     4
                  // 5  6  7
                  console.log('this.expectResize========', this.expectResize);
                  switch (this.expectResize) {
                    case 0:
                      this.selection.w = (oldx - p.mouseX) * 2;
                      this.selection.h = (oldy - p.mouseY) * 2;
                      break;
                    case 1:
                      this.selection.h = (oldy - p.mouseY) * 2; //can only change height 
                      break;
                    case 2:
                      this.selection.w = (p.mouseX - oldx) * 2;
                      this.selection.h = (oldy - p.mouseY) * 2;
                      break;
                    case 3:
                      this.selection.w = (oldx - p.mouseX) * 2; // can only change width
                      break;
                    case 4:
                      this.selection.w = (p.mouseX - oldx) * 2; // can only change width
                      break;
                    case 5:
                      this.selection.w = (oldx - p.mouseX) * 2;
                      this.selection.h = (p.mouseY - oldy) * 2;
                      break;
                    case 6:
                      this.selection.h = (p.mouseY - oldy) * 2; //can only change height
                      break;
                    case 7:
                      this.selection.w = (p.mouseX - oldx) * 2;
                      this.selection.h = (p.mouseY - oldy) * 2;
                      break;
                  }
                  break;
                case 'ellipse':
                case 'text':
                case 'triangle':

                  //       (c1)
                  // a     b
                  //       (c2)
                  console.log('this.expectResize triangle========', this.expectResize);
                  switch (this.expectResize) {
                    case 0:
                      this.selection.x = p.mouseX;
                      this.selection.y = p.mouseY;
                      break;
                    case 1:
                      this.selection.x4 = p.mouseX;
                      this.selection.y4 = p.mouseY;
                      break;
                  }
                  break;
                case 'line':
                  //       (c1)
                  // a     b
                  //       (c2)
                  console.log('this.expectResize line========', this.expectResize);
                  switch (this.expectResize) {
                    case 0:
                      this.selection.x = p.mouseX;
                      this.selection.y = p.mouseY;
                      break;
                    case 1:
                      this.selection.x4 = p.mouseX;
                      this.selection.y4 = p.mouseY;
                      break;
                  }
                  break;
                case 'scale':
                  //       (c1)
                  // a     b
                  //       (c2)
                  console.log('this.expectResize scale========', this.expectResize);
                  switch (this.expectResize) {
                    case 0:
                      this.selection.x = p.mouseX;
                      this.selection.y = p.mouseY;
                      break;
                    case 1:
                      this.selection.x4 = p.mouseX;
                      this.selection.y4 = p.mouseY;
                      break;
                  }
                  // if (this.scaleFlg && this.scaleCurrenNum == this.selNumber) {
                  if (this.scaleFlg) {
                    let dis = p.int(p.dist(this.selection.x, this.selection.y, this.selection.x4, this.selection.y4));
                    this.scale = Number((this.scaleInput.value() / dis).toFixed(2));
                  }
                  break;
                default:
                  break;
              }
            }
            this.valid = false; // Something's dragging so we must redraw

            return;
          }

        }

        this.expectResize = -1;
        p.cursor(p.ARROW);
        return;
      });

      p.mouseMoved = (() => {
        this.expectResize = -1;
        if (p.mouseX > p.width || p.mouseX < 0 || p.mouseY > p.height || p.mouseY < 0) {
          return;
        }

        if (this.selection !== undefined && this.selection !== null) {
          if (this.selected === 'drag') {
            if (this.selection.contains(p)) {
              p.cursor(p.MOVE);
            } else {
              p.cursor(p.ARROW);
            }
            this.valid = false;
            return;
          } else if (this.selected === 'resize') {
            // if (!this.resizeDragging) {
            if (this.selection.type === 'rect') {
              for (let i = 0; i < 8; i++) {
                // 0  1  2
                // 3     4
                // 5  6  7
                let cur = this.selection.selectionHandles[i];
                // we dont need to use the ghost context because
                // selection handles will always be rectangles
                if (p.mouseX >= cur.x && p.mouseX <= cur.x + this.selection.selectionBoxSize &&
                  p.mouseY >= cur.y && p.mouseY <= cur.y + this.selection.selectionBoxSize) {
                  // we found one!
                  this.expectResize = i;
                  p.cursor(p.CROSS);
                  this.valid = false;
                  return;
                } else {
                  p.cursor(p.ARROW);
                }
              }
            } else if (this.selection.type === 'triangle' || this.selection.type === 'line' || this.selection.type === 'scale') {
              //       (c1)
              // a       b
              //       (c2)
              for (let i = 0; i < 2; i++) {
                let cur = this.selection.selectionHandles[i];
                if (p.mouseX >= cur.x && p.mouseX <= cur.x + this.selection.selectionBoxSize &&
                  p.mouseY >= cur.y && p.mouseY <= cur.y + this.selection.selectionBoxSize) {
                  // we found one!
                  this.expectResize = i;
                  p.cursor(p.CROSS);
                  this.valid = false;
                  return;
                } else {
                  p.cursor(p.ARROW);
                }
              }
            }
            // }
          }
        }
      });

      p.mouseReleased = (() => {
        console.log('mouseReleased');

        if (this.selected === 'rotate') {

        } else {

        }
        this.resizeDragging = false;
        this.expectResize = -1;
        if (this.selection !== undefined && this.selection !== null) {
          if (this.selection.w < 0) {
            this.selection.w = -this.selection.w;
          }
          if (this.selection.h < 0) {
            this.selection.h = -this.selection.h;
          }
          this.selection.release();
          this.valid = false;
        }
      });
    }
    this.myP5 = new p5(p5obj);
  }

  clearCtx(p) {
    this.img = null;
    p.clear();
    p.background(200);
  }
  myInputEvent(p5) {
    console.log('you are typing: ', p5.value());
  }

  deleteShape() {
    if (this.selection !== undefined && this.selection !== null) {
      console.log(this.selNumber);
      this.myShapes.splice(this.selNumber, 1);
      console.log(this.myShapes.length);
      this.valid = false;
    } else {
      alert("削除したい図形を選択してください。");
    }
  }

}
