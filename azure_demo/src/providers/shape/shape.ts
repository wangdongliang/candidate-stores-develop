import { Injectable } from '@angular/core';

/*
  Generated class for the ShapeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Shape {
  private x;
  private y;
  private w;
  private h;

  private x2 = 0;
  private y2 = 0;
  private x3 = 0
  private y3 = 0;
  private x4 = 0
  private y4 = 0;
  private color = 'black';
  private type;
  private pressedX = 0;
  private pressedY = 0;
  private draggedFlg = false;
  private selectedFlg = false;
  private rotatedFlg = false;
  private rotAngle = 0;
  private selectionColorLine = '#00fffc';
  private selectionColor = '#FF00A8';
  private selectionWidth = 1;
  private selectionBoxSize = 8;
  private selectionBoxColor = 'darkred';
  private selectionHandles = [];
  private scale = 1;
  private scaleFlg = false;
  private scaleInput;

  constructor(p5, color, type, x?, y?, w?, h?, x4?, y4?, scale?, scaleFlg?, scaleInput?) {
    this.x = x || 0;
    this.y = y || 0;
    this.w = w || 1;
    this.h = h || 1;
    this.x4 = x4 || 0;
    this.y4 = y4 || 0;
    // this.fill = fill || '#ffffff';
    // this.stroke = stroke || '#000000';
    // this.strokeWeight = strokeWeight || 1;
    if (color != undefined && color != null) {
      this.color = color;
    }
    // this.color = p5.color(p5.random(255), p5.random(255), p5.random(255), 128);
    this.type = type || 'rect';
    this.pressedX = 0;
    this.pressedY = 0;
    this.draggedFlg = false;
    this.selectedFlg = false;
    this.rotatedFlg = false;
    this.rotAngle = 0;
    this.selectionColorLine = '#00fffc';
    this.selectionColor = '#FF00A8';
    this.selectionWidth = 1;
    this.selectionBoxSize = 8;
    this.selectionBoxColor = 'darkred';
    this.selectionHandles = [];
    if (this.scale != undefined && this.scale != null) {
      this.scale = scale;
    }
    this.scaleFlg = scaleFlg;
    this.scaleInput = scaleInput;
  }


  // Draws this shape to a given context
  public draw(ctx, color, p5) {
    p5.fill(255, 255, 255, 0);
    // p5.fill(this.color);
    console.log('this.selectedFlg=====', this.selectedFlg);
    if (this.selectedFlg) {
      p5.fill(155, 155, 0, 128);
    }
    if (this.draggedFlg) {
      p5.fill(200, 100, 200, 128);
    }

    if (this.type === 'rect') {
      for (let i = 0; i < 8; i += 1) {
        this.selectionHandles.push(new Shape(p5, color, 'rect'));
      }
      if (this.selectedFlg) {
        ctx.beginPath();
        // ctx.moveTo(0, this.y - 5);
        // ctx.lineTo(0, this.y + 5);
        // ctx.moveTo(0, this.y);
        // ctx.lineTo(this.x, this.y);
        // //y axis from 0 to y
        // ctx.lineTo(this.x, 0);
        // ctx.moveTo(this.x - 5, 0);
        // ctx.lineTo(this.x + 5, 0);
        // //h
        // ctx.moveTo(this.x - this.w / 2 + this.w / 2, this.y - this.h / 2);
        // ctx.lineTo(this.x - this.w / 2 + this.w / 2, this.y - this.h / 2 + this.h);
        // //w
        // ctx.moveTo(this.x - this.w / 2, this.y - this.h / 2 + this.h / 2);
        // ctx.lineTo(this.x - this.w / 2 + this.w, this.y - this.h / 2 + this.h / 2);

        //draw the align lines
        ctx.strokeStyle = this.selectionColorLine;
        ctx.lineWidth = 2;

        ctx.stroke();
        ctx.strokeStyle = this.selectionColor;
        ctx.lineWidth = this.selectionWidth;

        //draw the outline of the rect
        // rectMode(CENTER);
        // rect(this.x, this.y, this.w, this.h);

        // draw the boxes
        let half = this.selectionBoxSize / 2;

        // 0  1  2
        // 3     4
        // 5  6  7

        // top left, middle, right
        this.selectionHandles[0].x = this.x - this.w / 2 - half;
        this.selectionHandles[0].y = this.y - this.h / 2 - half;

        this.selectionHandles[1].x = this.x - this.w / 2 + this.w / 2 - half;
        this.selectionHandles[1].y = this.y - this.h / 2 - half;

        this.selectionHandles[2].x = this.x - this.w / 2 + this.w - half;
        this.selectionHandles[2].y = this.y - this.h / 2 - half;

        //middle left
        this.selectionHandles[3].x = this.x - this.w / 2 - half;
        this.selectionHandles[3].y = this.y - this.h / 2 + this.h / 2 - half;

        //middle right
        this.selectionHandles[4].x = this.x - this.w / 2 + this.w - half;
        this.selectionHandles[4].y = this.y - this.h / 2 + this.h / 2 - half;

        //bottom left, middle, right
        this.selectionHandles[6].x = this.x - this.w / 2 + this.w / 2 - half;
        this.selectionHandles[6].y = this.y - this.h / 2 + this.h - half;

        this.selectionHandles[5].x = this.x - this.w / 2 - half;
        this.selectionHandles[5].y = this.y - this.h / 2 + this.h - half;

        this.selectionHandles[7].x = this.x - this.w / 2 + this.w - half;
        this.selectionHandles[7].y = this.y - this.h / 2 + this.h - half;

        // ctx.fillStyle = this.selectionBoxColor;
        for (let i = 0; i < 8; i++) {
          let cur = this.selectionHandles[i];
          ctx.fillRect(cur.x, cur.y, this.selectionBoxSize, this.selectionBoxSize);
        }
      } else {
        ctx.stroke();
        ctx.lineWidth = 1;
        ctx.strokeStyle = this.color;
      }
    }

    if (this.type === 'triangle' || this.type === 'line' || this.type === 'scale') {
      for (let i = 0; i < 2; i += 1) {
        this.selectionHandles.push(new Shape(p5, color, 'rect'));
      }
      if (this.selectedFlg) {
        ctx.beginPath();
        // ctx.moveTo(0, this.y - 5);
        // ctx.lineTo(0, this.y + 5);
        // ctx.moveTo(0, this.y);
        // ctx.lineTo(this.x, this.y);
        // //y axis from 0 to y
        // ctx.lineTo(this.x, 0);
        // ctx.moveTo(this.x - 5, 0);
        // ctx.lineTo(this.x + 5, 0);
        // //h
        // ctx.moveTo(this.x - this.w / 2 + this.w / 2, this.y - this.h / 2);
        // ctx.lineTo(this.x - this.w / 2 + this.w / 2, this.y - this.h / 2 + this.h);
        // //w
        // ctx.moveTo(this.x - this.w / 2, this.y - this.h / 2 + this.h / 2);
        // ctx.lineTo(this.x - this.w / 2 + this.w, this.y - this.h / 2 + this.h / 2);

        //draw the align lines
        // ctx.strokeStyle = this.selectionColorLine;
        // ctx.lineWidth = 2;

        // ctx.stroke();
        // ctx.strokeStyle = this.selectionColor;
        // ctx.lineWidth = this.selectionWidth;

        //draw the outline of the rect
        // rectMode(CENTER);
        // rect(this.x, this.y, this.w, this.h);

        // draw the boxes
        let half = this.selectionBoxSize / 2;

        //       (c1)
        // a     b
        //       (c2)

        // top left, middle, right
        this.selectionHandles[0].x = this.x - half;
        this.selectionHandles[0].y = this.y - half;

        this.selectionHandles[1].x = this.x4 - half;
        this.selectionHandles[1].y = this.y4 - half;

        // ctx.fillStyle = this.selectionBoxColor;
        for (let i = 0; i < 2; i++) {
          let cur = this.selectionHandles[i];
          ctx.fillRect(cur.x, cur.y, this.selectionBoxSize, this.selectionBoxSize);
        }
      }
      // } else {
      // ctx.stroke();
      // ctx.lineWidth = 1;
      // ctx.strokeStyle = this.color;
      // ctx.strokeStyle = 'black';
      // }
    }

    switch (this.type) {
      case 'rect':
        p5.push();
        p5.translate(this.x, this.y);
        p5.rectMode(p5.CENTER);
        if (this.rotatedFlg) {
          p5.rotate(this.rotAngle);
        }
        p5.rect(0, 0, this.w, this.h);
        p5.pop();
        break;
      case 'line':
        p5.push();
        p5.stroke(this.color);
        p5.strokeWeight(2);
        p5.fill('black');
        p5.line(this.x, this.y, this.x4, this.y4);
        p5.noStroke();
        p5.fill('black');
        p5.text((Math.round(p5.int(p5.dist(this.x, this.y, this.x4, this.y4)) * 10) / 10 * this.scale).toFixed(1), (this.x4 + this.x) / 2, (this.y4 + this.y) / 2 - 3);
        p5.pop();
        break;
      case 'scale':
        p5.push();
        p5.strokeWeight(2);
        p5.stroke('blue');
        p5.line(this.x, this.y, this.x4, this.y4);
        p5.noStroke();
        p5.fill('black');
        p5.text(this.scaleInput + 'm', (this.x4 + this.x) / 2, (this.y4 + this.y) / 2 - 3);
        break;
      case 'triangle':
        //       c1
        // a     b
        //       c2
        let ax = this.x;
        let ay = this.y;
        let bx = this.x4;
        let by = this.y4;

        console.log("(" + ax + "," + ay + ")" + " (" + bx + "," + by + ")")
        //θを定義する(radian)
        let theta = p5.radians(25);
        console.log("theta=" + (theta / Math.PI) + "π");
        //l(AB間)を求める。
        let l = Math.sqrt((ax - bx) * (ax - bx) + (by - ay) * (by - ay))
        console.log("l=" + l);
        //r(AC間)を求める
        let r = l / p5.cos(theta);
        console.log("r=" + r);
        //αを求める(radian)
        let alpha = Math.atan2(by - ay, bx - ax);
        console.log("alpha=" + alpha + " tanα=" + p5.tan(alpha));
        //cの座標を求める
        let cx1 = r * p5.cos(alpha + theta) + ax;
        let cy1 = r * p5.sin(alpha + theta) + ay;
        console.log("(" + cx1 + "," + cy1 + ")");

        //cの座標を求める
        let cx2 = r * p5.cos(alpha - theta) + ax;
        let cy2 = r * p5.sin(alpha - theta) + ay;
        console.log("(" + cx2 + "," + cy2 + ")");
        //kの長さを求める
        let k = Math.sqrt((cx1 - bx) * (cx1 - bx) + (by - cy1) * (by - cy1));

        this.x = ax;
        this.y = ay;
        this.x2 = cx1;
        this.y2 = cy1;
        this.x3 = cx2;
        this.y3 = cy2;

        p5.push();
        p5.strokeWeight(2);
        p5.stroke(this.color);
        p5.triangle(ax, ay, bx, by, cx1, cy1);
        // p5.stroke(this.color);
        p5.triangle(ax, ay, bx, by, cx2, cy2);
        p5.stroke(this.color);
        p5.noStroke();
        p5.fill('black');
        p5.text((Math.round(l * 10) / 10 * this.scale).toFixed(1), (bx + ax) / 2, (by + ay) / 2 + 3);
        p5.text((Math.round(r * 10) / 10 * this.scale).toFixed(1), (cx1 + ax) / 2, (cy1 + ay) / 2 + 3);
        p5.text((Math.round(k * 10) / 10 * this.scale).toFixed(1), (bx + cx1) / 2, (by + cy1) / 2 + 3);
        p5.text((Math.round(r * 10) / 10 * this.scale).toFixed(1), (cx2 + ax) / 2, (cy2 + ay) / 2 + 3);
        p5.text((Math.round(k * 10) / 10 * this.scale).toFixed(1), (bx + cx2) / 2, (by + cy2) / 2 + 3);
        p5.pop();
        break;

      case 'ellipse':
        p5.ellipse(this.x, this.y, this.w, this.h);
        break;

      case 'text':
        p5.text('word', this.x, this.y);
        break;

      default:
        break;
    }
  }

  public rotate(p5) {
    this.rotAngle = p5.atan2(p5.mouseY - p5.height / 2, p5.mouseX - p5.width / 2);
    this.rotatedFlg = true;
  }

  // Determine if a point is inside the rect's bounds
  // Shape.prototype.contains = (mouseX, mouseY)=> {
  //     return (this.x <= mouseX) && (this.x + this.w >= mouseX) &&
  //         (this.y <= mouseY) && (this.y + this.h >= mouseY);
  // }

  public setColor(p5) {
    // this.color = p5.color(random(255), random(255), random(255));
  }

  public contains(p5) {
    switch (this.type) {
      case 'rect':
        if (p5.abs(this.x - p5.mouseX) <= this.w / 2 &&
          p5.abs(this.y - p5.mouseY) <= this.h / 2) {
          return true;
        }
      case 'line':
        if ((p5.mouseX - this.x) * (this.y4 - this.y) - (this.x4 - this.x) * (p5.mouseY - this.y) <= 300
          && p5.min(this.x, this.x4) <= p5.mouseX && p5.mouseX <= p5.max(this.x, this.x4)
          && p5.min(this.y, this.y4) <= p5.mouseY && p5.mouseY <= p5.max(this.y, this.y4)) {
          return true;
        } else {
          return false;
        }
      case 'scale':
        if ((p5.mouseX - this.x) * (this.y4 - this.y) - (this.x4 - this.x) * (p5.mouseY - this.y) <= 300
          && p5.min(this.x, this.x4) <= p5.mouseX && p5.mouseX <= p5.max(this.x, this.x4)
          && p5.min(this.y, this.y4) <= p5.mouseY && p5.mouseY <= p5.max(this.y, this.y4)) {
          return true;
        } else {
          return false;
        }
      case 'triangle':
        var vertx = [this.x, this.x2, this.x3];
        var verty = [this.y, this.y2, this.y3];
        var i, j, c = false;
        for (i = 0, j = 3 - 1; i < 3; j = i++) {
          if (((verty[i] > p5.mouseY) != (verty[j] > p5.mouseY)) &&
            (p5.mouseX < (vertx[j] - vertx[i]) * (p5.mouseY - verty[i]) / (verty[j] - verty[i]) + vertx[i])) {
            c = !c;
          }
        }
        return c;

      case 'ellipse':
        var dx = p5.mouseX - this.x;
        var dy = p5.mouseY - this.y;
        return (dx * dx) / (this.w * this.w) + (dy * dy) / (this.h * this.h) <= 1;
      case 'text':
        // text('word', this.x, this.y);
        break;
      default:
        break;
    }
    return false;
  }

  public pressed(p5) {
    switch (this.type) {
      case 'rect':
        if (p5.abs(this.x - p5.mouseX) <= this.w / 2 &&
          p5.abs(this.y - p5.mouseY) <= this.h / 2) {
          this.pressedX = this.x - p5.mouseX;
          this.pressedY = this.y - p5.mouseY;
          this.draggedFlg = true;
          this.selectedFlg = true;
        }
        return this.draggedFlg;

      case 'triangle':
        var vertx = [this.x, this.x2, this.x3];
        var verty = [this.y, this.y2, this.y3];
        var i, j, c = false;
        for (i = 0, j = 3 - 1; i < 3; j = i++) {
          if (((verty[i] > p5.mouseY) != (verty[j] > p5.mouseY)) &&
            (p5.mouseX < (vertx[j] - vertx[i]) * (p5.mouseY - verty[i]) / (verty[j] - verty[i]) + vertx[i])) {
            c = !c;
          }
        }
        this.draggedFlg = c;
        return c;
      case 'line':
        if ((p5.mouseX - this.x) * (this.y4 - this.y) - (this.x4 - this.x) * (p5.mouseY - this.y) <= 300
          && p5.min(this.x, this.x4) <= p5.mouseX && p5.mouseX <= p5.max(this.x, this.x4)
          && p5.min(this.y, this.y4) <= p5.mouseY && p5.mouseY <= p5.max(this.y, this.y4)) {
          return true;
        } else {
          return false;
        }
      case 'scale':
        if ((p5.mouseX - this.x) * (this.y4 - this.y) - (this.x4 - this.x) * (p5.mouseY - this.y) <= 300
          && p5.min(this.x, this.x4) <= p5.mouseX && p5.mouseX <= p5.max(this.x, this.x4)
          && p5.min(this.y, this.y4) <= p5.mouseY && p5.mouseY <= p5.max(this.y, this.y4)) {
          return true;
        } else {
          return false;
        }
      case 'ellipse':
        var dx = p5.mouseX - this.x;
        var dy = p5.mouseY - this.y;
        this.draggedFlg = (dx * dx) / (this.w * this.w) + (dy * dy) / (this.h * this.h) <= 1;

        return this.draggedFlg;
      case 'text':
        // text('word', this.x, this.y);
        break;
      default:
        break;
    }
  }

  public drag(p5) {
    if (this.draggedFlg) {
      switch (this.type) {
        case 'rect':
        case 'ellipse':
        case 'text':
          this.x = p5.mouseX + this.pressedX;
          this.y = p5.mouseY + this.pressedY;
          break;

        case 'triangle':
          this.x = p5.mouseX + this.pressedX;
          this.y = p5.mouseY - 50 + this.pressedY;
          break;
        default:
          break;
      }
    }
  }

  public release() {
    console.log('release');
    this.draggedFlg = false;
  }

}

export class Point {
  private x;
  private y;
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

export class Line {
  sPoint;
  ePoint;
  constructor(sPoint, ePoint) {
    this.sPoint = sPoint;
    this.ePoint = ePoint;
  }
}