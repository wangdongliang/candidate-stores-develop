import { Component } from '@angular/core';
import { File } from '@ionic-native/file';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PdfViewerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pdf-viewer',
  templateUrl: 'pdf-viewer.html',
})
export class PdfViewerPage {
  pdfSrc;
  zoom = 1.0;
  constructor(public navCtrl: NavController, public navParams: NavParams, private file: File) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PdfViewerPage');
  }

  onFileSelected(fileInput: Event) {

    let target: any = fileInput.target;
    let file = target.files[0];
    let fileName = file.name;

    let ext2 = "";
    let i = fileName.lastIndexOf('.');
    let p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
    if (i > p) {
      ext2 = fileName.substring(i + 1);
    }
    if (ext2.toLocaleLowerCase() == 'pdf') {
      let $img: any = document.querySelector('#file');

      if (typeof (FileReader) !== 'undefined') {
        let reader = new FileReader();

        reader.onload = (e: any) => {
          this.pdfSrc = e.target.result;
        };

        reader.onerror = (e: any) => {
          console.error("File could not be read! " + e.target.error);
        };

        reader.readAsArrayBuffer($img.files[0]);
      }
    }
    else {
      alert("すみません、ファイルタイプのミスは、pdfになっています。");
    }
  }

  zoomin() {
    this.zoom = this.zoom + 0.1;
  }

  zoomout() {
    this.zoom = this.zoom - 0.1;
  }

}
