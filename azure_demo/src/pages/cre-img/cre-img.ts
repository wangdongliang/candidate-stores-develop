import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { BlobServiceProvider } from '../../providers/blob-service/blob-service';
import { AuthProvider } from '../../providers/auth/auth';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file'

/**
 * Generated class for the CreImgPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cre-img',
  templateUrl: 'cre-img.html',
})
export class CreImgPage {

  private haspicture = false;
  private temp = '';
  private picture = 'assets/imgs/NoImage.png';
  private result;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public blobServiceProvider: BlobServiceProvider,
    public auth: AuthProvider,
    private camera: Camera,
    private imagePicker: ImagePicker,
    private file: File) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreImgPage');
  }

  takePicture() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // takePicture successed
      this.temp = imageData;
      this.haspicture = true
    }, (err) => {
      // Handle error
      this.haspicture = false
      this.showAlert('ERROR:' + err);
    });
  }

  // openImgPicker
  openImgPicker() {
    // parameter
    const imagePickerOpt = {
      maximumImagesCount: 1, //selected number
      width: 800,
      height: 800,
      quality: 80
    };

    this.imagePicker.getPictures(imagePickerOpt).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.temp = results[i];
      }

      document.getElementsByTagName('img')[0].setAttribute('src', this.temp);
      this.haspicture = true;
    }, (err) => {
      this.haspicture = false;
      this.showAlert('ERROR:' + err); //err
    });
  }

  private async success(file) {
    console.log("File size: " + file.size);
    let name = this.getPicName();
    let token = await this.auth.getToken(this.auth.storageResource);
    if (token) {
      console.log('this.auth.accessToken====', token);
      if (token) {
        let rlt = this.blobServiceProvider.uploadBlob('pocstores',
          token, 'pocstores', name, file,
          (err) => {
            if (err) {
              // Upload blob failed
              console.error('画像ファイルアップロードは失敗しました。');
              this.result = '画像ファイルアップロードは失敗しました。'
            } else {
              // Upload successfully
              console.log('画像ファイルアップロードは成功しました。');
              this.result = '画像ファイルアップロードは成功しました。'
            }
          });
      }
    } else {
      console.error('認証失敗');
    }

  }

  private fail(error) {
    alert("Unable to retrieve file properties: " + error.code);
    this.result = '画像ファイルアップロードは失敗しました。'
  }

  // uploadImg
  async clickMethod() {
    if (this.temp) {
      // this.accessToken = await this.storage.get('accessToken');
      this.file.resolveLocalFilesystemUrl(this.temp).then(function (entry) {
        if (entry.isFile) {
          entry.file(this.success.bind(this), this.fail);
        }
        else {
          console.log('it is not a file');
        }
      }.bind(this));
    } else {
      this.result = '画像ファイルは存在しない。'
    }
  }

  private showAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'エラー',
      subTitle: msg,
      buttons: ['確定']
    });
    alert.present();
  }

  private getPicName() {
    let date = new Date()
    let m = date.getMonth() + 1
    let d = date.getDate()
    let h = date.getHours()
    let f = date.getMinutes();
    let s = date.getSeconds();
    let str = date.getFullYear() + "_" + m + "_" + d + "_" + h + "_" + f + "_" + s

    return str
  }

}
