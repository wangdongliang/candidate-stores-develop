import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the PolygonDemoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare let google;

@IonicPage()
@Component({
  selector: 'page-polygon-demo',
  templateUrl: 'polygon-demo.html',
})

export class PolygonDemoPage {

  //Googlemap表示オブジェクト
  @ViewChild('map') mapElement: ElementRef;
  private map: any;
  private propertyKey: string;
  private ploygonArr = [];
  private listenerArr = [];
  private propertyVal: string;
  private properties = [];
  private isDisplay = 'block';

  constructor(private navCtrl: NavController, 
              private loadingCtrl:LoadingController) {
  }


  ionViewDidLoad() {
    this.initMap();
  }
  
  private initMap() {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      center: {lat: 35.459471, lng: 139.627371},
      zoom: 15,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false
    });
  }

  private setPropertyValue() {
    console.log('propertyKey:', this.propertyKey);
    this.cleanAll();

    switch(this.propertyKey) {
      case 'basic':
        this.properties = [];
        this.setPloygonWithBasic();
        break;
      case 'draggable':
        this.properties = [];
        this.properties = ['true', 'false'];
        break;
      case 'editable':
        this.properties = [];
        this.properties = ['true', 'false'];    
        break;
      case 'fillColor':
        this.properties = [];
        this.properties = ['red', 'blue', 'green'];
        break;
      case 'fillOpacity':
        this.properties = [];
        this.properties = ['0.5', '1'];
        break;
      case 'strokeColor':
        this.properties = [];
        this.properties = ['red', 'blue', 'green'];
        break;
      case 'strokeOpacity':
        this.properties = [];
        this.properties = ['0.5', '1'];
        break;
      case 'strokeWeight':
        this.properties = [];
        this.properties = ['2', '5', '8'];
        break;
    }
  }

  public changePropertyVal() {
    console.log('propertyVal:', this.propertyVal);
    this.cleanAll();

    switch(this.propertyKey) {
      case 'draggable':
        this.setPloygonWithDraggable();
        break;
      case 'editable':
        this.setPloygonWithEditable();        
        break;
      case 'fillColor':
        this.setPloygonWithFillColor();
        break;
      case 'fillOpacity':
        this.setPloygonWithFillOpacity();
        break;
      case 'strokeColor':
        this.setPloygonWithStrokeColor();
        break;
      case 'strokeOpacity':
        this.setPloygonWithStrokeOpacity();
        break;
      case 'strokeWeight':
        this.setPloygonWithStrokeWeight();
        break;
    }
  }

  private setPloygonWithBasic() {
    let ploygon = new google.maps.Polygon({
      map: this.map
    });

    let listener = this.map.addListener('click', (e)=> {
      console.log('map click e:', e);
      console.log('lat():', e.latLng.lat());
      console.log('lng():', e.latLng.lng());

      ploygon.getPath().push(e.latLng);
    });

    this.ploygonArr.push(ploygon);
    this.listenerArr.push(listener);
  }
  
  private setPloygonWithEditable() {
    console.log('Boolean(this.propertyVal)', Boolean(this.propertyVal));
    let ploygon = new google.maps.Polygon({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 3,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: this.map,
      editable: this.propertyVal === 'true'
    });

    let listener = this.map.addListener('click', (e)=> {
      console.log('map click e:', e);
      ploygon.getPath().push(e.latLng);
    });

    this.ploygonArr.push(ploygon);
    this.listenerArr.push(listener);
  }
  

  private setPloygonWithDraggable() {
    let ploygon = new google.maps.Polygon({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 3,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: this.map,
      draggable: this.propertyVal === 'true'
    });

    let listener = this.map.addListener('click', (e)=> {
      console.log('map click e:', e);
      ploygon.getPath().push(e.latLng);
    });

    this.ploygonArr.push(ploygon);
    this.listenerArr.push(listener);
  }

  private setPloygonWithFillColor() {

    let ploygon = new google.maps.Polygon({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 3,
      fillColor: this.propertyVal,
      fillOpacity: 0.35,
      map: this.map,
    });

    let listener = this.map.addListener('click', (e)=> {
      console.log('map click e:', e);
      ploygon.getPath().push(e.latLng);
    });

    this.ploygonArr.push(ploygon);
    this.listenerArr.push(listener);
  }

  private setPloygonWithFillOpacity() {

    let ploygon = new google.maps.Polygon({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 3,
      fillColor: '#FF0000',
      fillOpacity: this.propertyVal,
      map: this.map,
    });

    let listener = this.map.addListener('click', (e)=> {
      console.log('map click e:', e);
      ploygon.getPath().push(e.latLng);
    });

    this.ploygonArr.push(ploygon);
    this.listenerArr.push(listener);
  }

  private setPloygonWithStrokeColor() {

    let ploygon = new google.maps.Polygon({
      strokeColor: this.propertyVal,
      strokeOpacity: 0.8,
      strokeWeight: 3,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: this.map,
    });

    let listener = this.map.addListener('click', (e)=> {
      console.log('map click e:', e);
      ploygon.getPath().push(e.latLng);
    });

    this.ploygonArr.push(ploygon);
    this.listenerArr.push(listener);
  }

  private setPloygonWithStrokeOpacity() {

    let ploygon = new google.maps.Polygon({
      strokeColor: '#FF0000',
      strokeOpacity: this.propertyVal,
      strokeWeight: 3,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: this.map,
    });

    let listener = this.map.addListener('click', (e)=> {
      console.log('map click e:', e);
      ploygon.getPath().push(e.latLng);
    });

    this.ploygonArr.push(ploygon);
    this.listenerArr.push(listener);
  }

  private setPloygonWithStrokeWeight() {

    let ploygon = new google.maps.Polygon({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: this.propertyVal,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: this.map,
    });

    let listener = this.map.addListener('click', (e)=> {
      console.log('map click e:', e);
      ploygon.getPath().push(e.latLng);
    });

    this.ploygonArr.push(ploygon);
    this.listenerArr.push(listener);
  }

  public setDraggable() {
    this.ploygonArr.forEach(element => {
      element.setDraggable(!element.getDraggable())
    });
  }

  public setEditable() {
    this.ploygonArr.forEach(element => {
      element.setEditable(!element.getEditable())
    });
  }

  public setOptions() {
    this.ploygonArr.forEach(element => {
      element.setOptions({
        strokeColor: 'blue',
        strokeOpacity: 0.8,
        strokeWeight: 5,
        fillColor: 'blue',
        fillOpacity: 0.35,
        map: this.map,
      })
    });
  }

  public backspace() {
    this.ploygonArr.forEach(element => {
      element.getPath().pop();
    });
  }

  public drawCircle() {
    let circle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: this.map,
      center: {lat: 35.459471, lng: 139.627371},
      radius: 200,
      draggable: true,
      editable: true
    });

    this.ploygonArr.push(circle);
  }

  public drawRectangle() {
    let rectangle = new google.maps.Rectangle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: this.map,
      bounds: {
        north: 35.459471,
        south: 35.464571,
        east: 139.629571,
        west: 139.627371
      },
      draggable: true,
      editable: true
    });

    this.ploygonArr.push(rectangle);
  }

  public addTriggerToPoly() {
    this.cleanAll();
    let ploygon = new google.maps.Polygon({
      strokeColor: '#FF0000',
      strokeOpacity: '0.8',
      strokeWeight: '5',
      fillColor: '#FF0000',
      fillOpacity: '0.35',
      map: this.map,
      editable: true,
      draggable: true,
      path:[{lat: 35.46139353872545, lng: 139.6221782433472},
           {lat: 35.46083425947163, lng: 139.62870137567143},
           {lat: 35.45577608216479, lng: 139.6300506591797},
           {lat: 35.45730371943704, lng: 139.62312238092045}]
    });

    ploygon.addListener('click', ()=> {
      let infoWindow = new google.maps.InfoWindow();
      infoWindow.setContent('click event');
      infoWindow.setPosition({lat: 35.45730371943704, lng: 139.62312238092045});
      infoWindow.open(this.map);
    });

    ploygon.addListener('dragend', ()=> {
      let infoWindow = new google.maps.InfoWindow();
      infoWindow.setContent('dragend event');
      infoWindow.setPosition({lat: 35.46139353872545, lng: 139.6221782433472});
      infoWindow.open(this.map);
    });

    this.ploygonArr.push(ploygon);
  }

  public cleanAll() {
    this.ploygonArr.forEach(element => {
      element.setMap(null);  
    });

    this.listenerArr.forEach(element => {
      element.remove();  
    });
  }

  public isVisable() {
    this.isDisplay = this.isDisplay == 'none' ? 'block' : 'none';
  }

}
