import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as p5 from 'p5/lib/p5.min';

/**
 * Generated class for the P5Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-p5',
  templateUrl: 'p5.html',
})
export class P5Page {
  myp5: any;
  myp51: any;
  myp52: any;
  penColor: string; // ペンの色
  strokeWidth: number;  // ペンの太さRangeパーツで値が変動する
  drawFlag = false;
  x: number;
  y: number;
  w: number;
  h: number;
  graph: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.penColor = 'white';  // デフォルトは赤
    this.strokeWidth = 1;  // デフォルトの太さを10に設定

    let p5obj = (p) => {
      // console.log("x:", x);
      let dragging = false; // Is the object being dragged?
      let rollover = false; // Is the mouse over the ellipse?
      p.setup = (() => {
        var cnv = p.createCanvas(document.getElementById('myCanvas').clientWidth, document.getElementById('myCanvas').clientHeight);
        // id="myCanvas"と紐づけ
        cnv.parent('myCanvas');
        p.background('white');

      });
      // タッチイベントを受けて処理が走る。
      p.touchMoved = () => {
        // ペンの太さ
        p.strokeWeight(this.strokeWidth);
        // ペンの色
        p.stroke(this.penColor);
        // なぞったエリアに線を描画
        p.line(p.mouseX, p.mouseY, p.pmouseX, p.pmouseY);
        return false;
      };

    }
    this.myp5 = new p5(p5obj);

  }
  // 色を赤に
  onRed() {
    // タッチイベントを受けて処理が走る。
    this.penColor = 'red';
  }

  // 色を青に
  onBlue() {
    this.penColor = 'blue';
  }
  // 色を黄色に
  onYellow() {
    this.penColor = 'yellow';
  }
  // 色を白 に
  onWhite() {
    this.penColor = 'white';
  }
  save() {
    this.myp5.save('myCanvas.jpg')
  }
}

