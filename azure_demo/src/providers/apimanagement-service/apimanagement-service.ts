import { Injectable } from '@angular/core';
import { ApiCommonProvider } from '../api-common/api-common';

/*
  Generated class for the ApimanagementServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApimanagementServiceProvider {
  private path = '/student/';
  constructor(public commonService: ApiCommonProvider) {
    console.log('Hello ApimanagementServiceProvider Provider');
  }

  async createData(param) {
    // let path = '/create/';
    let res = await this.commonService.doHttp('doPut', this.path, param);
    return res;
  }

  async deleteData(id) {
    let path = this.path + '?studentId=';
    let res = await this.commonService.doHttp('doDelete', path + id);
    return res;
  }

  async editData(param) {
    let res = await this.commonService.doHttp('doPost', this.path, param);
    return res;
  }

  async selectData() {
    // let path = '/select/';
    let res = await this.commonService.doHttp('doGet', this.path);
    return res;
  }

}
