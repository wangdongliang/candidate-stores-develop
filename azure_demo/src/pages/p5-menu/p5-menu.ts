import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the P5MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-p5-menu',
  templateUrl: 'p5-menu.html',
})
export class P5MenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad P5MenuPage');
  }


  goLine() {
    this.navCtrl.push('P5Page');
  }
  goDrag() {
    this.navCtrl.push('P5_1Page');
  }
  goRotate() {
    this.navCtrl.push('P5_2Page');
  }

  goPaint() {
    this.navCtrl.push('PaintPage');
  }

}
