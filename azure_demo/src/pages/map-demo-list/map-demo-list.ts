import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MapDemoListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map-demo-list',
  templateUrl: 'map-demo-list.html',
})
export class MapDemoListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapDemoListPage');
  }

  // PolygonのDEMO
  navToPolygon(){
    this.navCtrl.push('PolygonDemoPage');
  }
  
  // PolyLineのDEMO
  navToPolyline() {
    this.navCtrl.push('PolylineDemoPage');
  }

  // CalculateのDEMO
  navToCalculate() {
    this.navCtrl.push('CalculateDemoPage');
  }

  navToSpeech() {
    this.navCtrl.push('SpeechPage');
  }

  navToStaticMap() {
    this.navCtrl.push('MapImageDemoPage');
  }
  
}
