import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PolylineDemoPage } from './polyline-demo';

@NgModule({
  declarations: [
    PolylineDemoPage,
  ],
  imports: [
    IonicPageModule.forChild(PolylineDemoPage),
  ],
})
export class PolylineDemoPageModule {}
