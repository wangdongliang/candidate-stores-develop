import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalculateDemoPage } from './calculate-demo';

@NgModule({
  declarations: [
    CalculateDemoPage,
  ],
  imports: [
    IonicPageModule.forChild(CalculateDemoPage),
  ],
})
export class CalculateDemoPageModule {}
