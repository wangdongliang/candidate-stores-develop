import { Injectable } from '@angular/core';
import { MSAdal, AuthenticationContext, AuthenticationResult } from '@ionic-native/ms-adal';
import { ViewCommonProvider } from '../../providers/view-common/view-common';
import { AlertController } from 'ionic-angular';
import { errorHandler } from '@angular/platform-browser/src/browser';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  private authContext: AuthenticationContext;
  private accessToken: string;
  private authority = 'https://login.windows.net/b0228deb-e421-4b18-966a-f5f74a6b11cc';
  public storageResource = 'https://storage.azure.com/';
  public apiResource = 'https://management.core.windows.net/';
  private clientId = 'd67b309c-a0f3-4705-bca5-1212c53bc7ab';
  private redirectUrl = 'https://localhost';

  constructor(public alertCtrl: AlertController, public msAdal: MSAdal) {
    console.log('Hello AuthProvider Provider');
  }

  public async initContext(resourceUrl) {
    this.authContext = this.msAdal.createAuthenticationContext(this.authority);
    this.clearCache();

    console.log('認証開始');
    await this.authContext.acquireTokenAsync(resourceUrl, this.clientId, this.redirectUrl, '', '')
      .then(this.authCompletedCallback.bind(this))
      .catch((e: any) => {
        console.error('認証失敗', e);
        this.authContext = null;
        this.accessToken = null;
      });
  }

  public async getToken(resourceUrl) {
    if (this.authContext == undefined || this.authContext == null) {
      console.info('未認証');
      let token = await new Promise((resolve, reject) => {
        ViewCommonProvider.showConfirmationAlert(
          this.alertCtrl,
          '未認証です',
          '「はい」を押下して、ご認証お願い致します。',
          async () => {
            console.log('「はい」を押下');
            await this.initContext(resourceUrl);
            resolve(this.accessToken);
          },
          async () => {
            console.log('未認証です');
          }
        );
      });

      return token;

    } else {
      let token = await this.acquireToken(resourceUrl);
      console.log(token);

      return token;
    }
  }

  private async acquireToken(resourceUrl) {
    console.log(this.authContext);
    let items = await this.authContext.tokenCache.readItems();

    if (items.length > 0) {
      let authority = items[0].authority;
      this.authContext = this.msAdal.createAuthenticationContext(authority);
    }

    try {
      let res = await this.authContext.acquireTokenSilentAsync(resourceUrl, this.clientId, '');
      this.accessToken = res.accessToken;
      console.log(this.accessToken);
      return this.accessToken;
    } catch (err) {
      console.log(err, '再認証してください。');
      try {
        let retryRes = await this.authContext.acquireTokenAsync(resourceUrl, this.clientId, this.redirectUrl, '', '');
        this.accessToken = retryRes.accessToken;
        console.log(this.accessToken);

        return this.accessToken;
      } catch (retryErr) {
        this.errorCallback(retryErr);
      }
    }
  }

  private authCompletedCallback(authResponse: AuthenticationResult) {
    console.log('Token is', authResponse.accessToken);
    console.log('Token will expire on', authResponse.expiresOn);
    this.accessToken = authResponse.accessToken;
    ViewCommonProvider.showErrorAlert(this.alertCtrl, '認証成功しました。');
  }

  private errorCallback(e: any) {
    console.error(e, '認証失敗');

    this.clearCache();
    this.authContext = null;
    this.accessToken = null;
  }

  public clearCache() {
    try {
      this.authContext.tokenCache.clear();
      console.log('this.authContext.tokenCache is cleared');
    } catch (error) {
      console.log(error);
    }
  }

}
