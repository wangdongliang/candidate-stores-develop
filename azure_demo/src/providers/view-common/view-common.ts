import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class ViewCommonProvider {

  public static showErrorAlert(alertCtrl: AlertController, msg, fun?) {
    let alert = alertCtrl.create({
      title: msg,
      buttons: [
        {
          text: 'OK',
          handler: fun
        }
      ]
    });
    alert.present();
  }

  public static showConfirmationAlert(alertCtrl: AlertController, sTitle, msg, fnYes, fnNo) {
    let confirm = alertCtrl.create({
      title: sTitle,
      message: msg,
      buttons: [
        {
          text: 'いいえ',
          handler: fnNo
        },
        {
          text: 'はい',
          handler: fnYes
        }
      ]
    });
    confirm.present();
  }

}
