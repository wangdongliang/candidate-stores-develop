import { Http, Headers, ResponseContentType } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthProvider } from '../auth/auth';
import { ViewCommonProvider } from '../view-common/view-common';
import { Observable } from 'rxjs';

/**
 * 
 * 概 要： AZUREの通信処理
 * 作成日：2018.06.15
 * 作成者：DHC徐磊健
 */
@Injectable()
export class ApiCommonProvider {
  private host = 'https://poc-stores.azure-api.net';
  constructor(public http: Http, public auth: AuthProvider) {
    console.log('Hello ApiCommonProvider Provider');
  }

  async doHttp(httpMethod, path: string, param?: Object) {
    let token = await this.auth.getToken(this.auth.apiResource);
    if (token) {
      console.log('this.auth.accessToken====', token);
      let res = null;
      switch (httpMethod) {
        case 'doGet':
          res = await this.doGet(path, token);
          return res;
        case 'doDelete':
          res = await this.doDelete(path, token);
          return res;
        case 'doPost':
          res = await this.doPost(path, param, token);
          return res;
        case 'doPut':
          res = await this.doPut(path, param, token);
          return res;
        default:
          break;
      }
    } else {
      console.error('認証失敗');
    }
  }

  // select
  async doGetWithHeader(path: string, tokenHeaderParam, resType?) {
    try {

      let opstion = {
        headers: tokenHeaderParam,
        responseType: resType
      }
      return await this.http.get(path, opstion).toPromise();

    } catch (error) {
      console.log("http get error:" + error);
      return error;
    }
  }

  // delete
  async doDeleteWithHeader(path: string, tokenHeaderParam, resType?) {
    try {

      let opstion = {
        headers: tokenHeaderParam,
        responseType: resType
      }
      console.log('opstion', opstion);
      return await this.http.delete(path, opstion).toPromise();

    } catch (error) {
      console.log("http delete error:" + error);
      return error;
    }
  }

  // update
  async doPostWithHeader(path: string, formData, tokenHeaderParam, resType?) {
    try {

      let opstion = {
        headers: tokenHeaderParam,
        responseType: resType
      }
      console.log('opstion', opstion);
      return await this.http.post(path, formData, opstion).toPromise();
    } catch (error) {
      console.log("http post error:" + error);
      return error;
    }
  }

  // select
  async doGet(path: string, accessToken) {
    let addHeaders = new Headers({ 'Authorization': accessToken, 'Content-Type': 'application/json' });
    try {
      let res = await this.http.get(this.host + path, { headers: addHeaders }).toPromise();
      return res;
    } catch (error) {
      console.log("http get error:" + error);
      return error;
    }
  }

  // delete
  async doDelete(path: string, accessToken) {
    let addHeaders = new Headers({ 'Authorization': accessToken, 'Content-Type': 'application/json' });
    try {
      let res = await this.http.delete(this.host + path, { headers: addHeaders }).toPromise();
      let result = res.json();
      return result;
    } catch (error) {
      console.log("http delete error:" + error);
      return error;
    }
  }

  // update
  async doPost(path: string, param: Object, accessToken) {
    console.log('update');
    let addHeaders = new Headers({ 'Authorization': accessToken, 'Content-Type': 'application/json' });
    try {
      let res = await this.http.post(this.host + path, JSON.stringify(param), { headers: addHeaders }).toPromise();
      let result = res.json();
      return result;
    } catch (error) {
      console.log("http post error:" + error);
      return error;
    }
  }

  // create
  async doPut(path: string, param: Object, accessToken) {
    console.log('create');
    let addHeaders = new Headers({ 'Authorization': accessToken, 'Content-Type': 'application/json' });
    try {
      let res = await this.http.put(this.host + path, JSON.stringify(param), { headers: addHeaders }).toPromise();
      let result = res.json();
      return result;
    } catch (error) {
      console.log("http put error:" + error);
      return error;
    }
  }
}
